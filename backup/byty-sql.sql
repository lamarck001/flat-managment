-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: byty
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('accessSite','1',1443424183),('accessSite','2',1442949208),('accessUserAdmin','1',1443424183),('createFlat','1',1443424183),('manageOwnFlats','1',1443424183),('manageOwnFlats','2',1442956802),('superadmin','1',1442947512);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1442949136,1442949136),('/admin/*',2,NULL,NULL,NULL,1442949126,1442949126),('/admin/assignment/*',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/assignment/assign',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/assignment/index',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/assignment/revoke',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/assignment/view',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/default/*',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/default/index',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/*',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/item/add-child',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/create',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/delete',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/index',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/remove-child',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/item/update',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/item/view',2,NULL,NULL,NULL,1442949122,1442949122),('/admin/menu/*',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/create',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/delete',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/index',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/update',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/values',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/menu/view',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/route/*',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/route/add',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/route/index',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/route/remove',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/rule/*',2,NULL,NULL,NULL,1442949126,1442949126),('/admin/rule/create',2,NULL,NULL,NULL,1442949126,1442949126),('/admin/rule/delete',2,NULL,NULL,NULL,1442949126,1442949126),('/admin/rule/index',2,NULL,NULL,NULL,1442949124,1442949124),('/admin/rule/update',2,NULL,NULL,NULL,1442949126,1442949126),('/admin/rule/view',2,NULL,NULL,NULL,1442949124,1442949124),('/datecontrol/*',2,NULL,NULL,NULL,1442949128,1442949128),('/datecontrol/parse/*',2,NULL,NULL,NULL,1442949128,1442949128),('/datecontrol/parse/convert',2,NULL,NULL,NULL,1442949128,1442949128),('/debug/*',2,NULL,NULL,NULL,1442949130,1442949130),('/debug/default/*',2,NULL,NULL,NULL,1442949130,1442949130),('/debug/default/db-explain',2,NULL,NULL,NULL,1442949128,1442949128),('/debug/default/download-mail',2,NULL,NULL,NULL,1442949128,1442949128),('/debug/default/index',2,NULL,NULL,NULL,1442949128,1442949128),('/debug/default/toolbar',2,NULL,NULL,NULL,1442949128,1442949128),('/debug/default/view',2,NULL,NULL,NULL,1442949128,1442949128),('/document-templates/*',2,NULL,NULL,NULL,1442949132,1442949132),('/document-templates/ajax-get-document',2,NULL,NULL,NULL,1442949132,1442949132),('/document-templates/create',2,NULL,NULL,NULL,1442949130,1442949130),('/document-templates/delete',2,NULL,NULL,NULL,1442949132,1442949132),('/document-templates/index',2,NULL,NULL,NULL,1442949130,1442949130),('/document-templates/update',2,NULL,NULL,NULL,1442949132,1442949132),('/document-templates/view',2,NULL,NULL,NULL,1442949130,1442949130),('/document/*',2,NULL,NULL,NULL,1442949130,1442949130),('/document/ajax-delete',2,NULL,NULL,NULL,1442949130,1442949130),('/document/ajax-save-document',2,NULL,NULL,NULL,1442949130,1442949130),('/flat/*',2,NULL,NULL,NULL,1442949132,1442949132),('/flat/create',2,NULL,NULL,NULL,1442949132,1442949132),('/flat/delete',2,NULL,NULL,NULL,1442949132,1442949132),('/flat/index',2,NULL,NULL,NULL,1442949132,1442949132),('/flat/update',2,NULL,NULL,NULL,1442949132,1442949132),('/flat/view',2,NULL,NULL,NULL,1442949132,1442949132),('/gii/*',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/*',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/action',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/diff',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/index',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/preview',2,NULL,NULL,NULL,1442949130,1442949130),('/gii/default/view',2,NULL,NULL,NULL,1442949130,1442949130),('/gridview/*',2,NULL,NULL,NULL,1442949128,1442949128),('/gridview/export/*',2,NULL,NULL,NULL,1442949128,1442949128),('/gridview/export/download',2,NULL,NULL,NULL,1442949128,1442949128),('/rbac/*',2,NULL,NULL,NULL,1442949128,1442949128),('/rbac/assignment/*',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/assignment/assign',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/permission/*',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/permission/create',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/permission/delete',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/permission/index',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/permission/update',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/role/*',2,NULL,NULL,NULL,1442949128,1442949128),('/rbac/role/create',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/role/delete',2,NULL,NULL,NULL,1442949128,1442949128),('/rbac/role/index',2,NULL,NULL,NULL,1442949126,1442949126),('/rbac/role/update',2,NULL,NULL,NULL,1442949126,1442949126),('/record/*',2,NULL,NULL,NULL,1442949134,1442949134),('/record/ajax-delete',2,NULL,NULL,NULL,1442949134,1442949134),('/record/create',2,NULL,NULL,NULL,1442949132,1442949132),('/record/delete',2,NULL,NULL,NULL,1442949132,1442949132),('/record/index',2,NULL,NULL,NULL,1442949132,1442949132),('/record/update',2,NULL,NULL,NULL,1442949132,1442949132),('/record/view',2,NULL,NULL,NULL,1442949132,1442949132),('/redactor/*',2,NULL,NULL,NULL,1442949128,1442949128),('/site/*',2,NULL,NULL,NULL,1442949134,1442949134),('/site/about',2,NULL,NULL,NULL,1442949134,1442949134),('/site/captcha',2,NULL,NULL,NULL,1442949134,1442949134),('/site/contact',2,NULL,NULL,NULL,1442949134,1442949134),('/site/error',2,NULL,NULL,NULL,1442949134,1442949134),('/site/index',2,NULL,NULL,NULL,1442949134,1442949134),('/site/login',2,NULL,NULL,NULL,1442949134,1442949134),('/site/logout',2,NULL,NULL,NULL,1442949134,1442949134),('/subject/*',2,NULL,NULL,NULL,1442949136,1442949136),('/subject/create',2,NULL,NULL,NULL,1442949134,1442949134),('/subject/delete',2,NULL,NULL,NULL,1442949134,1442949134),('/subject/index',2,NULL,NULL,NULL,1442949134,1442949134),('/subject/update',2,NULL,NULL,NULL,1442949134,1442949134),('/subject/view',2,NULL,NULL,NULL,1442949134,1442949134),('/tenant/*',2,NULL,NULL,NULL,1442949136,1442949136),('/tenant/create',2,NULL,NULL,NULL,1442949136,1442949136),('/tenant/delete',2,NULL,NULL,NULL,1442949136,1442949136),('/tenant/index',2,NULL,NULL,NULL,1442949136,1442949136),('/tenant/update',2,NULL,NULL,NULL,1442949136,1442949136),('/tenant/view',2,NULL,NULL,NULL,1442949136,1442949136),('/user/*',2,NULL,NULL,NULL,1442949122,1442949122),('/user/admin/*',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/assignments',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/block',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/confirm',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/create',2,NULL,NULL,NULL,1442949111,1442949111),('/user/admin/delete',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/index',2,NULL,NULL,NULL,1442949111,1442949111),('/user/admin/info',2,NULL,NULL,NULL,1442949112,1442949112),('/user/admin/update',2,NULL,NULL,NULL,1442949111,1442949111),('/user/admin/update-profile',2,NULL,NULL,NULL,1442949112,1442949112),('/user/profile/*',2,NULL,NULL,NULL,1442949112,1442949112),('/user/profile/index',2,NULL,NULL,NULL,1442949112,1442949112),('/user/profile/show',2,NULL,NULL,NULL,1442949112,1442949112),('/user/recovery/*',2,NULL,NULL,NULL,1442949119,1442949119),('/user/recovery/request',2,NULL,NULL,NULL,1442949112,1442949112),('/user/recovery/reset',2,NULL,NULL,NULL,1442949112,1442949112),('/user/registration/*',2,NULL,NULL,NULL,1442949119,1442949119),('/user/registration/confirm',2,NULL,NULL,NULL,1442949119,1442949119),('/user/registration/connect',2,NULL,NULL,NULL,1442949119,1442949119),('/user/registration/register',2,NULL,NULL,NULL,1442949119,1442949119),('/user/registration/resend',2,NULL,NULL,NULL,1442949119,1442949119),('/user/security/*',2,NULL,NULL,NULL,1442949119,1442949119),('/user/security/auth',2,NULL,NULL,NULL,1442949119,1442949119),('/user/security/login',2,NULL,NULL,NULL,1442949119,1442949119),('/user/security/logout',2,NULL,NULL,NULL,1442949119,1442949119),('/user/settings/*',2,NULL,NULL,NULL,1442949122,1442949122),('/user/settings/account',2,NULL,NULL,NULL,1442949119,1442949119),('/user/settings/confirm',2,NULL,NULL,NULL,1442949119,1442949119),('/user/settings/disconnect',2,NULL,NULL,NULL,1442949119,1442949119),('/user/settings/networks',2,NULL,NULL,NULL,1442949119,1442949119),('/user/settings/profile',2,NULL,NULL,NULL,1442949119,1442949119),('accessSite',2,'This user can access site',NULL,NULL,1442947541,1443424241),('accessUserAdmin',2,'',NULL,NULL,1442947596,1443424251),('createFlat',2,NULL,NULL,NULL,1443424082,1443424082),('manageOwnFlats',2,'','isFlatCreator',NULL,1442956776,1443473840),('superadmin',1,'Super administrator of system',NULL,NULL,1442947497,1443429232);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('superadmin','/*'),('accessUserAdmin','/admin/*'),('superadmin','/admin/*'),('accessUserAdmin','/admin/assignment/*'),('superadmin','/admin/assignment/*'),('accessUserAdmin','/admin/assignment/assign'),('superadmin','/admin/assignment/assign'),('accessUserAdmin','/admin/assignment/index'),('superadmin','/admin/assignment/index'),('accessUserAdmin','/admin/assignment/revoke'),('superadmin','/admin/assignment/revoke'),('accessUserAdmin','/admin/assignment/view'),('superadmin','/admin/assignment/view'),('accessUserAdmin','/admin/default/*'),('superadmin','/admin/default/*'),('accessUserAdmin','/admin/default/index'),('superadmin','/admin/default/index'),('accessUserAdmin','/admin/item/*'),('superadmin','/admin/item/*'),('accessUserAdmin','/admin/item/add-child'),('superadmin','/admin/item/add-child'),('accessUserAdmin','/admin/item/create'),('superadmin','/admin/item/create'),('accessUserAdmin','/admin/item/delete'),('superadmin','/admin/item/delete'),('accessUserAdmin','/admin/item/index'),('superadmin','/admin/item/index'),('accessUserAdmin','/admin/item/remove-child'),('superadmin','/admin/item/remove-child'),('accessUserAdmin','/admin/item/update'),('superadmin','/admin/item/update'),('accessUserAdmin','/admin/item/view'),('superadmin','/admin/item/view'),('accessUserAdmin','/admin/menu/*'),('superadmin','/admin/menu/*'),('accessUserAdmin','/admin/menu/create'),('superadmin','/admin/menu/create'),('accessUserAdmin','/admin/menu/delete'),('superadmin','/admin/menu/delete'),('accessUserAdmin','/admin/menu/index'),('superadmin','/admin/menu/index'),('accessUserAdmin','/admin/menu/update'),('superadmin','/admin/menu/update'),('accessUserAdmin','/admin/menu/values'),('superadmin','/admin/menu/values'),('accessUserAdmin','/admin/menu/view'),('superadmin','/admin/menu/view'),('accessUserAdmin','/admin/route/*'),('superadmin','/admin/route/*'),('accessUserAdmin','/admin/route/add'),('superadmin','/admin/route/add'),('accessUserAdmin','/admin/route/index'),('superadmin','/admin/route/index'),('accessUserAdmin','/admin/route/remove'),('superadmin','/admin/route/remove'),('accessUserAdmin','/admin/rule/*'),('superadmin','/admin/rule/*'),('accessUserAdmin','/admin/rule/create'),('superadmin','/admin/rule/create'),('accessUserAdmin','/admin/rule/delete'),('superadmin','/admin/rule/delete'),('accessUserAdmin','/admin/rule/index'),('superadmin','/admin/rule/index'),('accessUserAdmin','/admin/rule/update'),('superadmin','/admin/rule/update'),('accessUserAdmin','/admin/rule/view'),('superadmin','/admin/rule/view'),('superadmin','/datecontrol/*'),('superadmin','/datecontrol/parse/*'),('superadmin','/datecontrol/parse/convert'),('superadmin','/debug/*'),('superadmin','/debug/default/*'),('superadmin','/debug/default/db-explain'),('superadmin','/debug/default/download-mail'),('superadmin','/debug/default/index'),('superadmin','/debug/default/toolbar'),('superadmin','/debug/default/view'),('superadmin','/document-templates/*'),('superadmin','/document-templates/ajax-get-document'),('superadmin','/document-templates/create'),('superadmin','/document-templates/delete'),('superadmin','/document-templates/index'),('superadmin','/document-templates/update'),('superadmin','/document-templates/view'),('superadmin','/document/*'),('superadmin','/document/ajax-delete'),('superadmin','/document/ajax-save-document'),('manageOwnFlats','/flat/*'),('superadmin','/flat/*'),('manageOwnFlats','/flat/create'),('superadmin','/flat/create'),('manageOwnFlats','/flat/delete'),('superadmin','/flat/delete'),('manageOwnFlats','/flat/index'),('superadmin','/flat/index'),('manageOwnFlats','/flat/update'),('superadmin','/flat/update'),('manageOwnFlats','/flat/view'),('superadmin','/flat/view'),('superadmin','/gii/*'),('superadmin','/gii/default/*'),('superadmin','/gii/default/action'),('superadmin','/gii/default/diff'),('superadmin','/gii/default/index'),('superadmin','/gii/default/preview'),('superadmin','/gii/default/view'),('superadmin','/gridview/*'),('superadmin','/gridview/export/*'),('superadmin','/gridview/export/download'),('superadmin','/rbac/*'),('superadmin','/rbac/assignment/*'),('superadmin','/rbac/assignment/assign'),('superadmin','/rbac/permission/*'),('superadmin','/rbac/permission/create'),('superadmin','/rbac/permission/delete'),('superadmin','/rbac/permission/index'),('superadmin','/rbac/permission/update'),('superadmin','/rbac/role/*'),('superadmin','/rbac/role/create'),('superadmin','/rbac/role/delete'),('superadmin','/rbac/role/index'),('superadmin','/rbac/role/update'),('superadmin','/record/*'),('superadmin','/record/ajax-delete'),('superadmin','/record/create'),('superadmin','/record/delete'),('superadmin','/record/index'),('superadmin','/record/update'),('superadmin','/record/view'),('superadmin','/redactor/*'),('accessSite','/site/*'),('superadmin','/site/*'),('accessSite','/site/about'),('superadmin','/site/about'),('accessSite','/site/captcha'),('superadmin','/site/captcha'),('accessSite','/site/contact'),('superadmin','/site/contact'),('accessSite','/site/error'),('superadmin','/site/error'),('accessSite','/site/index'),('superadmin','/site/index'),('accessSite','/site/login'),('superadmin','/site/login'),('accessSite','/site/logout'),('superadmin','/site/logout'),('superadmin','/subject/*'),('superadmin','/subject/create'),('superadmin','/subject/delete'),('superadmin','/subject/index'),('superadmin','/subject/update'),('superadmin','/subject/view'),('manageOwnFlats','/tenant/*'),('superadmin','/tenant/*'),('manageOwnFlats','/tenant/create'),('superadmin','/tenant/create'),('manageOwnFlats','/tenant/delete'),('superadmin','/tenant/delete'),('manageOwnFlats','/tenant/index'),('superadmin','/tenant/index'),('manageOwnFlats','/tenant/update'),('superadmin','/tenant/update'),('manageOwnFlats','/tenant/view'),('superadmin','/tenant/view'),('superadmin','/user/*'),('accessUserAdmin','/user/admin/*'),('superadmin','/user/admin/*'),('accessUserAdmin','/user/admin/assignments'),('superadmin','/user/admin/assignments'),('accessUserAdmin','/user/admin/block'),('superadmin','/user/admin/block'),('accessUserAdmin','/user/admin/confirm'),('superadmin','/user/admin/confirm'),('accessUserAdmin','/user/admin/create'),('superadmin','/user/admin/create'),('accessUserAdmin','/user/admin/delete'),('superadmin','/user/admin/delete'),('accessUserAdmin','/user/admin/index'),('superadmin','/user/admin/index'),('accessUserAdmin','/user/admin/info'),('superadmin','/user/admin/info'),('accessUserAdmin','/user/admin/update'),('superadmin','/user/admin/update'),('accessUserAdmin','/user/admin/update-profile'),('superadmin','/user/admin/update-profile'),('superadmin','/user/profile/*'),('superadmin','/user/profile/index'),('superadmin','/user/profile/show'),('superadmin','/user/recovery/*'),('superadmin','/user/recovery/request'),('superadmin','/user/recovery/reset'),('superadmin','/user/registration/*'),('superadmin','/user/registration/confirm'),('superadmin','/user/registration/connect'),('superadmin','/user/registration/register'),('superadmin','/user/registration/resend'),('superadmin','/user/security/*'),('superadmin','/user/security/auth'),('accessSite','/user/security/login'),('superadmin','/user/security/login'),('accessSite','/user/security/logout'),('superadmin','/user/security/logout'),('superadmin','/user/settings/*'),('superadmin','/user/settings/account'),('superadmin','/user/settings/confirm'),('superadmin','/user/settings/disconnect'),('superadmin','/user/settings/networks'),('superadmin','/user/settings/profile'),('superadmin','accessSite'),('superadmin','accessUserAdmin'),('superadmin','createFlat'),('superadmin','manageOwnFlats');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('isFlatCreator','O:24:\"app\\rbac\\FlatCreatorRule\":3:{s:4:\"name\";s:13:\"isFlatCreator\";s:9:\"createdAt\";i:1443428896;s:9:\"updatedAt\";i:1443428896;}',1443428896,1443428896);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `flat_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document_flat_id` (`flat_id`),
  KEY `fk_document_tenant_id` (`tenant_id`),
  CONSTRAINT `fk_document_flat_id` FOREIGN KEY (`flat_id`) REFERENCES `flat` (`id`),
  CONSTRAINT `fk_document_tenant_id` FOREIGN KEY (`tenant_id`) REFERENCES `tenant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (62,'Zmluva Redai','<p>Declarabgfnghnnces – BOMcheck<br></p> <p>We have registered as a supplier on BOMcheck and will provide a detailed declaration of all substances as listed in the “BOMcheck Lvbmnvbn</p><p>We will enter the information on BOMcheck using the SIEMENS HEALTHCARE part number or alternatively provide a mapping vbnvbn</p><p>If compliance with Substance Regulations requires a modification to the PRODUCTS we deliver to SIEMENS HEALTHCARE, we will inform SIEMENS HEALTHCARE in writing and within reasonable time prior to the first delivery of the modified products - independent of this declaration and in accordance with any other agreements.</p> <p>We have registered as a supplier on BOMcheck, using the following DUNS number:</p><table> <tbody> <tr> <td> DUNS (used for BOMcheck registration) </td> <td> </td> </tr> </tbody> </table> <p>We name as a contact person for all matters regarding product-related environmental protection:</p><table> <tbody> <tr> <td> Technical contact person: </td> <td> </td> </tr> <tr> <td> Telephone: </td> <td> </td> </tr> <tr> <td> Email: </td> <td> </td> </tr> </tbody> </table> <p>We confirm with our signature(s) the correctness of our statements set out above:</p><table> <tbody> <tr> <td> Manufacturer/supplier: </td> <td> </td> </tr> <tr> <td> Street address/P.O. box: </td> <td> </td> </tr> <tr> <td> Zip code, city: </td> <td> </td> </tr> <tr> <td> Date, Place signature(s): </td> <td> </td> </tr> <tr> <td> Printed name(s),<br> position(s): </td> <td> </td> </tr> </tbody> </table> <p>* This currently covers (but might in the future not be limited to) “Siemens Healthcare GmbH (Germany)”, “Siemens Magnet Technology Ltd. (United Kingdom)”, “Siemens Medical Solutions, Inc. (USA)”, “Siemens Shanghai Medical Equipment Ltd. (China)”, “Siemens Shenzhen Magnetic Resonance Ltd. (China)”, “Siemens X-Ray Vacuum Technology Ltd. (China)”, “Siemens Ltd., GOA Works (India)”, “Siemens Healthcare Diagnostics (worldwide)”, “Siemens Ltd. Seoul (Ultrasound Korea)”, „Siemens S.A. (GETAFE Spain)“, „Siemens Acrorad Co., Ltd. (Japan)“</p><p>** The “BOMcheck List of Restricted and Declarable Substances” can be accessed using the following link:<br> <a href=\"https://bomcheck.net/suppliers/restricted-and-declarable-substances-list\">https://bomcheck.net/suppliers/restricted-and-declarable-substances-list</a></p>',1,1,'2015-09-19 11:32:17','2015-09-19 11:32:17','Zmluva Redai-1442655137.pdf');
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_templates`
--

DROP TABLE IF EXISTS `document_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_templates`
--

LOCK TABLES `document_templates` WRITE;
/*!40000 ALTER TABLE `document_templates` DISABLE KEYS */;
INSERT INTO `document_templates` VALUES (1,'Zmluva','Zmluva xy medzi A a B'),(2,'Upomienka','Vážený {tenant}Tu bude nejaky text bla bla bla bla'),(3,'Zmluva 2','\r\n<div class=\"document-content\">\r\n    <table class=\"table header-table\">\r\n        <tr>\r\n            <td style=\"width:80%;\"><img src=\"../../../modules/bomcheck/img/document-siemens-logo.jpg\" /></td>\r\n            <td style=\"width:20%; text-align:right;\">\r\n                Supplier IFA.:</br>\r\n                Supplier Name:\r\n            </td>\r\n        </tr>\r\n    </table>\r\n\r\n\r\n\r\n    <p class=\"instruction-small\">\r\n        <span>This document must be completed and signed by an authorized representative of your company. </span><br />\r\n        <span>Please print, sign, scan and return via mail or email as received within 4 weeks.</span>\r\n    </p>\r\n\r\n    <h1>Declarable Substances – BOMcheck</h1>\r\n\r\n    <p>We have registered as a supplier on BOMcheck and will provide a detailed declaration of all substances as listed in the “BOMcheck List of Restricted and Declarable Substances”** in the web database BOMcheck (<a href=\"http://www.BOMcheck.net\">www.BOMcheck.net</a>) for PRODUCTS and packaging delivered to SIEMENS HEALTHCARE (including its affiliated companies)* after request of SIEMENS HEALTHCARE within 4 weeks. For forthcoming PRODUCTS and packaging delivered to SIEMENS HEALTHCARE, we will enter the required information into the BOMcheck database no later than the first delivery.</p>\r\n\r\n    <p>We will enter the information on BOMcheck using the SIEMENS HEALTHCARE part number or alternatively provide a mapping on BOMcheck between the part-number we use for the declaration and the corresponding SIEMENS HEALTHCARE part number.</p>\r\n\r\n    <p>Furthermore we ensure that we will keep our substance declaration in BOMcheck up-to-date. This also includes that we will update the substance declaration of a PRODUCT and packaging within 4 weeks if there are changes to the “BOMcheck List of Restricted and Declarable Substances”** or to the delivered PRODUCTS and packaging.</p>\r\n\r\n    <div class=\"box\">\r\n        <strong><u>Important:</u></strong>\r\n        <p>If compliance with Substance Regulations requires a modification to the PRODUCTS we deliver to SIEMENS HEALTHCARE, we will inform SIEMENS HEALTHCARE in writing and within reasonable time prior to the first delivery of the modified products - independent of this declaration and in accordance with any other agreements.</p>\r\n    </div>\r\n\r\n\r\n\r\n    <p>We have registered as a supplier on BOMcheck, using the following DUNS number:<p>\r\n\r\n    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >\r\n        <tbody>\r\n            <tr>\r\n                <td width=\"170\">                \r\n                    DUNS (used for BOMcheck registration)                    \r\n                </td>\r\n                <td >\r\n                   \r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n\r\n    <p>We name as a contact person for all matters regarding product-related environmental protection:<p>\r\n\r\n    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >\r\n        <tbody>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">\r\n                    Technical contact person:                   \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">              \r\n                    Telephone:                   \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">               \r\n                    Email:                    \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                    \r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n\r\n    <p>We confirm with our signature(s) the correctness of our statements set out above:<p>\r\n\r\n    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >\r\n        <tbody>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">               \r\n                    Manufacturer/supplier:                  \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">\r\n                    Street address/P.O. box:                \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">               \r\n                    Zip code, city:                    \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">\r\n                    Date, Place signature(s):\r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                    \r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"170\" valign=\"bottom\">                \r\n                    Printed name(s),<br/>\r\n                    position(s):                    \r\n                </td>\r\n                <td width=\"457\" valign=\"bottom\">\r\n                   \r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n\r\n    <div class=\"document-footer\">\r\n        <p>* This currently covers (but might in the future not be limited to) “Siemens Healthcare GmbH (Germany)”, “Siemens Magnet Technology Ltd. (United Kingdom)”,\r\n            “Siemens Medical Solutions, Inc. (USA)”, “Siemens Shanghai Medical Equipment Ltd. (China)”, “Siemens Shenzhen Magnetic Resonance Ltd. (China)”, “Siemens\r\n            X-Ray Vacuum Technology Ltd. (China)”, “Siemens Ltd., GOA Works (India)”, “Siemens Healthcare Diagnostics (worldwide)”, “Siemens Ltd. Seoul (Ultrasound\r\n            Korea)”, „Siemens S.A. (GETAFE Spain)“, „Siemens Acrorad Co., Ltd. (Japan)“</p>\r\n\r\n        <p>** The “BOMcheck List of Restricted and Declarable Substances” can be accessed using the following link:<br/>\r\n        <a href=\"https://bomcheck.net/suppliers/restricted-and-declarable-substances-list\">https://bomcheck.net/suppliers/restricted-and-declarable-substances-list</a></p>\r\n    </div>\r\n</div>'),(4,'ziados o','Vazeny pan {tenant} , ziadam o ;lsakfjlasj l; j ;slajosad sda');
/*!40000 ALTER TABLE `document_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flat`
--

DROP TABLE IF EXISTS `flat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account` int(11) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL DEFAULT '1442945907',
  `updated_at` int(11) NOT NULL DEFAULT '1442945907',
  `updated_by` int(11) NOT NULL DEFAULT '1',
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_price` int(11) DEFAULT NULL,
  `current_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flat_account_id` (`account`),
  CONSTRAINT `fk_flat_account_id` FOREIGN KEY (`account`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flat`
--

LOCK TABLES `flat` WRITE;
/*!40000 ALTER TABLE `flat` DISABLE KEYS */;
INSERT INTO `flat` VALUES (1,'Krušnohorská','','Krušnohorská','1664','Jirkov','',1,1442945907,1444432864,1,'154728','Czech republic',NULL,NULL),(2,'Kamenná 5104','','Kamenná ','5104','Chomutov','',2,1442945907,1442945907,1,NULL,NULL,NULL,NULL),(3,'Náš byt','Tu bude popis bytu','Vořarská','8','Praha','Czech',1,1442945907,1443556255,1,'14300','Czech republic',NULL,NULL),(4,'andrejov byt','','dade v BB','125445','Bystrica','',2,1442945907,1442945907,1,NULL,NULL,NULL,NULL),(5,'Na Borku 25','','Na Borku','741','Jirkov','',1,1443553699,1444983252,1,'','Czech',NULL,NULL);
/*!40000 ALTER TABLE `flat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'nove',NULL,'/user/admin/index',NULL,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1441478818),('m140209_132017_init',1442945678),('m140403_174025_create_account_table',1442945679),('m140504_113157_update_tables',1442945680),('m140504_130429_create_token_table',1442945681),('m140506_102106_rbac_init',1442947399),('m140602_111327_create_menu_table',1442948550),('m140830_171933_fix_ip_field',1442945682),('m140830_172703_change_account_table_name',1442945682),('m141222_110026_update_ip_field',1442945683),('m141222_135246_alter_username_length',1442945683),('m150614_103145_update_social_account_table',1442945685),('m150623_212711_fix_username_notnull',1442945686),('m150905_184736_flat',1441480616),('m150905_191724_tenant',1441481267),('m150906_162853_record',1441572398),('m150906_204410_record_categories',1441648437),('m150917_184223_document_templates',1442515603),('m150917_193147_documents',1442518357),('m150917_195012_documents_update',1442520379),('m150917_212527_documents_update2',1442525502),('m150918_051041_tenant_update',1442553361),('m150918_211835_documents_update3',1442611250),('m150919_123343_subject',1442691585),('m150922_210429_flat_update',1442956101),('m150928_072043_flat_update2',1443553549),('m150929_204548_subject_update',1443559677);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'Martin Kolárik','mkolarik@imsro.sk','','d41d8cd98f00b204e9800998ecf8427e','Prague','http://www.imsro.sk',''),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record`
--

DROP TABLE IF EXISTS `record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `category` int(11) NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `due` date DEFAULT NULL,
  `done` date DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sum` int(11) NOT NULL,
  `flat_id` int(11) NOT NULL,
  `responsible` int(2) NOT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_record_flat` (`flat_id`),
  KEY `fk_record_categories` (`category`),
  CONSTRAINT `fk_record_categories` FOREIGN KEY (`category`) REFERENCES `record_categories` (`id`),
  CONSTRAINT `fk_record_flat` FOREIGN KEY (`flat_id`) REFERENCES `flat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record`
--

LOCK TABLES `record` WRITE;
/*!40000 ALTER TABLE `record` DISABLE KEYS */;
INSERT INTO `record` VALUES (1,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,1,1,1),(2,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,1,2,1),(3,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,1,2,1),(4,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',1541,1,1,1),(5,1,1,'2001-02-20','2001-02-20',NULL,NULL,'AKjlsakd s',1541,1,1,0),(6,1,1,'2001-02-20','2001-02-20',NULL,NULL,'dfsgsdf',1200,1,1,0),(7,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,2,1,1),(8,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,2,1,1),(9,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',7500,2,1,1),(10,1,1,'2001-02-20','2001-02-20',NULL,NULL,'Poznamka',1200,2,1,1),(11,2,2,'2015-00-02','2015-00-02',NULL,NULL,'',520,2,2,0),(12,1,11,'2015-00-01','2015-00-30',NULL,NULL,'',1500,1,1,1),(13,2,3,'2015-00-01','2015-00-30',NULL,NULL,'',5410,1,1,1),(14,1,11,'2015-00-01','2015-00-04',NULL,NULL,'',421,1,1,0),(15,1,2,'2015-00-02','2015-00-11',NULL,NULL,'',452,1,2,0),(16,1,3,'2015-00-03','2015-00-10',NULL,NULL,'',452,1,2,0),(17,1,1,'2015-00-10','2015-00-11',NULL,NULL,'',44,1,2,0),(18,2,2,'2015-10-07','2015-10-14',NULL,NULL,'',521,1,1,0),(19,2,2,'2015-10-07','2015-10-14',NULL,NULL,'',521,1,1,0),(20,2,2,'2015-10-07','2015-10-14',NULL,NULL,'',521,1,1,0),(21,1,1,'2015-09-30','2015-10-01',NULL,NULL,'wert',77,5,1,0),(22,1,1,'2015-09-30','2015-10-01',NULL,NULL,'wert',77,5,1,1),(23,1,1,'2015-09-30','2015-10-15',NULL,NULL,'dddddd',1111,1,1,1);
/*!40000 ALTER TABLE `record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record_categories`
--

DROP TABLE IF EXISTS `record_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record_categories`
--

LOCK TABLES `record_categories` WRITE;
/*!40000 ALTER TABLE `record_categories` DISABLE KEYS */;
INSERT INTO `record_categories` VALUES (1,'TUV'),(2,'společné osvětlení'),(3,'teplo'),(4,'fond oprav'),(5,'správa'),(6,'pojištení'),(7,'úklid'),(8,'výtah'),(9,'studená voda'),(10,'daň'),(11,'nájomné'),(12,'preplatok'),(13,'nedoplatok'),(14,'iné');
/*!40000 ALTER TABLE `record_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_account`
--

DROP TABLE IF EXISTS `social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text,
  `code` varchar(32) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_account`
--

LOCK TABLES `social_account` WRITE;
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
INSERT INTO `social_account` VALUES (1,1,'github','2612765','{\"login\":\"lamarck001\",\"id\":2612765,\"avatar_url\":\"https:\\/\\/avatars.githubusercontent.com\\/u\\/2612765?v=3\",\"gravatar_id\":\"\",\"url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\",\"html_url\":\"https:\\/\\/github.com\\/lamarck001\",\"followers_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/followers\",\"following_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/following{\\/other_user}\",\"gists_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/gists{\\/gist_id}\",\"starred_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/starred{\\/owner}{\\/repo}\",\"subscriptions_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/subscriptions\",\"organizations_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/orgs\",\"repos_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/repos\",\"events_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/events{\\/privacy}\",\"received_events_url\":\"https:\\/\\/api.github.com\\/users\\/lamarck001\\/received_events\",\"type\":\"User\",\"site_admin\":false,\"name\":\"Martin\",\"company\":\"In Motion\",\"blog\":\"http:\\/\\/www.imsro.sk\",\"location\":\"Prague\",\"email\":\"mkolarik@imsro.sk\",\"hireable\":null,\"bio\":null,\"public_repos\":2,\"public_gists\":0,\"followers\":0,\"following\":0,\"created_at\":\"2012-10-21T13:52:00Z\",\"updated_at\":\"2015-09-22T20:46:58Z\",\"private_gists\":0,\"total_private_repos\":0,\"owned_private_repos\":0,\"disk_usage\":0,\"collaborators\":0,\"plan\":{\"name\":\"free\",\"space\":976562499,\"collaborators\":0,\"private_repos\":0}}',NULL,NULL,NULL,NULL),(2,1,'google','117283091504304040985','{\"kind\":\"plus#person\",\"etag\":\"\\\"1qbqD04eP9-SRJUBAhcE_i06Pdo\\/DmPw0H1TwXdbUwhsqKRyYSYeJi0\\\"\",\"nickname\":\"kolo\",\"gender\":\"male\",\"emails\":[{\"value\":\"topanka2000@seznam.cz\",\"type\":\"account\"}],\"objectType\":\"person\",\"id\":\"117283091504304040985\",\"displayName\":\"Martin Kol\\u00e1rik\",\"name\":{\"familyName\":\"Kol\\u00e1rik\",\"givenName\":\"Martin\"},\"url\":\"https:\\/\\/plus.google.com\\/117283091504304040985\",\"image\":{\"url\":\"https:\\/\\/lh4.googleusercontent.com\\/-L4rMb_6nyzY\\/AAAAAAAAAAI\\/AAAAAAAAANA\\/XPdrQzxTZtk\\/photo.jpg?sz=50\",\"isDefault\":false},\"isPlusUser\":true,\"language\":\"en\",\"circledByCount\":30,\"verified\":false}',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_type` int(2) NOT NULL,
  `type` int(2) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_street_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_type` int(100) DEFAULT NULL,
  `id_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_subject_user_id` (`user_id`),
  CONSTRAINT `fk_subject_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,1,1,'','Kamenná','412','Chomutov','97401','','','','','1213456','','David','Redai',NULL,'1',1,'',1,NULL,NULL,'Je to cigan prasivy',0,1),(5,1,1,'','Vorařská','8','Praha 4','14300','Czech Republic','','','','','','Martin','Kolárik',NULL,'1',1,'',1,NULL,NULL,'',1,1),(6,1,1,'','','','','','','','','','','','Dakto','Dalsiu',NULL,'1',1,'',1,NULL,NULL,'',1,1),(7,1,1,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,'kl','fgcfb',NULL,'1',1,NULL,5,NULL,NULL,'',1,1);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `sex` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_type` int(100) NOT NULL,
  `id_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `flat_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `address_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_street_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_tennat_flat` (`flat_id`),
  CONSTRAINT `fk_tennat_flat` FOREIGN KEY (`flat_id`) REFERENCES `flat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant`
--

LOCK TABLES `tenant` WRITE;
/*!40000 ALTER TABLE `tenant` DISABLE KEYS */;
INSERT INTO `tenant` VALUES (1,'','David','Redai','2015-00-02','2',3,'1233646',2,'2015-00-02',NULL,'Ciganova','541/15','Chomutov','97401','CZ','je to cigan'),(3,'Mr.','Ales','Ruzicka','2015-00-08','1',1,'124587458',1,'2015-00-03',NULL,'','','','','','');
/*!40000 ALTER TABLE `tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,'Wr2LQPl1_73HOjJ_aEL1Fg-_Y_CnkojX',1442945907,0),(2,'7vq8q7AbJdf9UYhA5eOEGRxYk5sDqkgn',1442946711,0);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`),
  UNIQUE KEY `user_unique_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'lamarck','mkolarik@imsro.sk','$2y$10$2j2d9fQFcYdVIqborVLSMeOK119ohF7kCM1I7DE5NTOBC0KUsqyJe','2Wd73ukFWmV5L38seRcytaj-OK8ej-_k',1442945987,NULL,NULL,'::1',1442945907,1442945907,0),(2,'andrej','andrej@imsro.sk','$2y$10$D31VcF8ICfodn82LLfV9xed2/livzZERZVzQPXIRc.1/8E9M2u4ae','k18ohuYNKHRLSSQeGBfHVbKVXYIrICcB',1442946730,NULL,NULL,'::1',1442946711,1442946711,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-29 11:04:18
