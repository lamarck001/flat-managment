<?php

$params = require(__DIR__ . '/params.php');

$config = [
   'id' => 'basic',
   'basePath' => dirname(__DIR__),
   'bootstrap' => [
      'log',
      'admin'
   ],
   'components' => [
     'request' => [         
         'cookieValidationKey' => 'efg97nh68dfsfglodfgb',
     ],
     'cache' => [
         'class' => 'yii\caching\FileCache',
     ],
     'errorHandler' => [
         'errorAction' => 'site/error',
     ],
     'mailer' => [
         'class' => 'yii\swiftmailer\Mailer',
         'useFileTransport' => true,
     ],
     'log' => [
         'traceLevel' => YII_DEBUG ? 3 : 0,
         'targets' => [
             [
                 'class' => 'yii\log\FileTarget',
                 'levels' => ['error', 'warning'],
             ],
         ],
     ],
      'urlManager' => [
         'enablePrettyUrl' => true,
         'showScriptName' => false,
         'rules' => [
            'flat/<id:\d+>' => 'flat/view'
         ]
      ],
      'authManager' => [
         'class' => 'yii\rbac\DbManager',
      ],

      'authClientCollection' => [
         'class'   => \yii\authclient\Collection::className(),
         'clients' => [
            'google' => [
                'class'        => 'dektrium\user\clients\Google',
                'clientId'     => '87175102918-vsfjjtmrtaklv7jipguor9845b52rif9.apps.googleusercontent.com',
                'clientSecret' => 'BLWqVbkvFWz7Chn4eJF3_cGC',
            ],
            'github' => [
                'class'        => 'dektrium\user\clients\GitHub',
                'clientId'     => 'd57e4f056d1ea94e73a8',
                'clientSecret' => '0b3150e43d0bcdbab0eae76c9802490105017c8b',
            ],
         ],
      ],

     'db' => require(__DIR__ . '/db.php'),
   ],
   'as access' => [
     'class' => 'mdm\admin\classes\AccessControl',
     'allowActions' => [
         'site/*',
         'admin/*', 
         'user/registration/*'
     ]
   ],
    
      'modules' => [
         'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['lamarck']
         ],
         'admin' => [
            'class' => 'mdm\admin\Module',         
         ],
         //'rbac' => [
         //   'class' => 'dektrium\rbac\Module',
         //],
         'gridview' => [
            'class' => 'kartik\grid\Module',
         ],    
         'redactor' => 'yii\redactor\RedactorModule',    
         'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',
            'autoWidget' => true,

         ]

    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    
    $config['modules']['gii']['generators'] = [
        'kartikgii-crud' => ['class' => 'warrence\kartikgii\crud\Generator'],
    ];

}

return $config;
