<?php

namespace app\controllers;

use Yii;
use app\models\Document;
use kartik\mpdf\Pdf;
use yii\web\NotFoundHttpException;

class DocumentController extends \yii\web\Controller
{
    public function actionSave()
    {   
        $timestamp = time();

        $id = Yii::$app->request->post('id');
        if($this->documentExists($id)) {          
          $model = $this->findModel($id);
        } else {
          $model = new Document;
        }

        $model->pdf = $this->savePdfDocument(Yii::$app->request->post(), $timestamp);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {  
          return json_encode(Yii::$app->request->post());
        } else {
          return json_encode($model->errors);
        }

    }

   public function cleanString($string)
   {
       $r = [
        '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
        '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
        '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
        'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
        'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
        'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
        'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
        'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
        'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
        'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
        'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
        'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
        'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
        'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
        'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
        '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
        'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
        'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
        'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
        'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
        'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
        'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
        'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
        'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
        'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
        'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
        'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
        'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
        '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
        'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
        'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
        'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
        'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
        'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
        'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
        'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
        'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
        'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
        'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
        'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
        'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
        'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
        'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
        'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
        'ю' => 'yu', 'я' => 'ya','ť'=>'t'
      ];
    
      $string = strtr($string, $r);

      return $string;
   }

   public function savePdfDocument($doc,$timestamp)
   {        
     //$title = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $doc['Document']['title']); 
      //$title = preg_replace("#[[:punct:]]#", "", $doc['Document']['title']);
      //$title = preg_replace("/[:punct:]+/", "", $doc['Document']['title']);
      //$title = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $doc['Document']['title']);

      $title = $this->cleanString($doc['Document']['title']);

      $fileName = Yii::getAlias('@app').'/documents/'.$title.'-'. $timestamp.'.pdf';

      $pdf = new Pdf([
         // set to use core fonts only
         'mode' => Pdf::MODE_CORE,
         // A4 paper format
         'format' => Pdf::FORMAT_A4,
         // portrait orientation
         'orientation' => Pdf::ORIENT_PORTRAIT,
         // stream to browser inline
         'destination' => Pdf::DEST_FILE,
         'filename'=>$fileName,
         // 'tempPath'=>''
         // your html content input
         'content' => $doc['Document']['text'],
         // format content from your own css file if needed or use the
         // enhanced bootstrap css built by Krajee for mPDF formatting
         //'cssFile' => '@frontend/web/css/document-pdf.css',
         // any css to be embedded if required
         // 'cssInline' => '.kv-heading-1{font-size:18px}',
         // set mPDF properties on the fly
         // 'options' => ['title' => 'Krajee Report Title'],
         // call mPDF methods on the fly
         /* 'methods' => [
            'SetHeader'=>['Krajee Report Header'],
            'WriteHTML'=> [$data['html'], 2],
            'SetFooter'=>['{PAGENO}'],

         ]*/
      ]);

      $pdf->render();

      return $title.'-'. $timestamp.'.pdf';
   }

     /**
     * Deletes an existing Record model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        rename(Yii::getAlias('@app').'/documents/'.$model->pdf, Yii::getAlias('@app').'/documents/deleted/'.$model->pdf);

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);          
    }

    public function actionDownload($id)
    {
      $model = $this->findModel($id);
      $file = Yii::getAlias('@app').'/documents/'.$model->pdf;
      if (file_exists($file)) {
        return Yii::$app->response->sendFile($file);
      }
    }

    public function actionEdit()
    {
      $id = Yii::$app->request->post('id');
      $model = $this->findModel($id);      
      Yii::$app->response->format = 'json';
      return $model;
    }


   /**
   * Finds the Flat model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Flat the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
   protected function findModel($id)
   {
     if (($model = Document::findOne($id)) !== null) {
         return $model;
     } else {
         throw new NotFoundHttpException('The requested page does not exist.');
     }
   }


   protected function documentExists($id)
   {
      return Document::findOne($id) !== null ? true : false ;         
   }

}
