<?php

namespace app\controllers;

use app\models\Payment;
use Yii;
use app\models\Flat;
use app\models\FlatSearch; 

use app\models\Record;
use app\models\RecordSearch;

use app\models\Subject;
use app\models\SubjectSearch;

use app\models\Document;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * FlatController implements the CRUD actions for Flat model.
 */
class FlatController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

   /**
   * Lists all Flat models.
   * @return mixed
   */
   public function actionIndex()
   {   
      $uid = Yii::$app->user->id;

      $searchModel = new FlatSearch;
      $dataProvider = $searchModel->searchCurrent(Yii::$app->request->getQueryParams(), $uid);

      return $this->render('index', [
         'dataProvider' => $dataProvider,
         'searchModel' => $searchModel,
      ]);
   }

   public function canHeDoIt($model)
   {
      return Yii::$app->user->can('manageOwnFlats', ['flat' => $model]) ?: $this->redirect(['index']);
   }

    /**
     * Displays a single Flat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    { 

      $model = $this->findModel($id);

      $this->canHeDoIt($model);

      //if (Yii::$app->user->can('manageOwnFlats', ['flat' => $model])) {      
     
         //$uid = Yii::$app->user->id;
         
         //record
         $recordModel = new Record;
         $recordSearchModel = new RecordSearch;
         $recordDataProvider = $recordSearchModel->searchCurrent(Yii::$app->request->getQueryParams(), $id);

         //tenant
         $tenantModel = new Subject;
         $tenantSearchModel = new SubjectSearch;
         $tenantDataProvider = $tenantSearchModel->searchCurrent(Yii::$app->request->getQueryParams(), $id);
    

         $documentModel = new Document;
         $documentDataProvider = $documentModel->searchCurrentDocuments($id);

         $currentTenant = Subject::find()->where('flat_id=:fid',[':fid'=>$id])->one();

         if(Yii::$app->request->post() ) {
           
           //echo '<pre>'; print_r(Yii::$app->request->post()); echo '</pre>'; 

           $recordModel->load(Yii::$app->request->post());
           $recordModel->save();
         }

        /*if ($recordModel->load(Yii::$app->request->post()) && $recordModel->save()) {         
            return $this->redirect(['index']);
         } else {*/
            return $this->render('view', [
               'model' => $model,
               'recordModel'=>$recordModel,
               'recordSearchModel'=>$recordSearchModel,
               'recordDataProvider'=> $recordDataProvider,
               'tenantModel'=>$tenantModel,
               'tenantDataProvider'=> $tenantDataProvider,
               'currentTenant'=> $currentTenant,               
               'documentModel'=>$documentModel,
               'documentDataProvider'=>$documentDataProvider,
                'paymentModel'=> new Payment
            ]);
         // }
     /* } else {
         return $this->redirect(['index']);         
      }*/

    }

    /**
     * Creates a new Flat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Flat;

        //$this->canHeDoIt($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
               'model' => $model             
            ]);
        }
    }

    /**
     * Updates an existing Flat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$isAjax=false)
    {
        $model = $this->findModel($id);
        $this->canHeDoIt($model);

        $records = Record::find()->where('flat_id=:id', [':id'=>$id])->all();
        $recordModel = new Record;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($isAjax){
               return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
               'model' => $model,
               'records'=> $records,
               'recordModel'=>$recordModel
            ]);
        }
    }

    /**
     * Deletes an existing Flat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Flat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Flat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Flat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
