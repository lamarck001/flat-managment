<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Payment;


class PaymentController extends Controller
{


    public function actionCreate()
    {
        $model = new Payment;
        $load = $model->load(Yii::$app->request->post());

        if ($load && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {

            /*var_dump($model->errors);
            die();*/


            //return $this->redirect(Yii::$app->request->referrer);
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

}

?>