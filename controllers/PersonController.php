<?php

namespace app\controllers;

use Yii;
use app\models\Person;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubjectController implements the CRUD actions for Subject model.
 */
class PersonController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionUpdate($id,$isAjax=false)
    {
        $model = $this->findModel($id);

        echo '<pre>'; var_dump(Yii::$app->request->post()); echo '</pre>';



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($isAjax){
               return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->redirect(['index']);
        } else {

            //echo '<pre>'; var_dump($model->errors); echo '</pre>';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Finds the Subject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
