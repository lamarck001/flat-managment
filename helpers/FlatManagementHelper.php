<?php
namespace app\helpers;

use Yii;
use yii\helpers\ArrayHelper;

use app\models\Contract;

class FlatManagementHelper
{

    public static function _paymentDetail($payment)
    {

        return [
            'type' => $payment->type,
            'text' => $payment->text,
            'from' => $payment->from,
            'to' => $payment->to,
            'due' => $payment->due,
            'created' => $payment->created,
            'finished' => $payment->finished,
            'note' => $payment->note,
            'variable_symbol' => $payment->variable_symbol,
            'method' => $payment->method,
            'type2' => $payment->type2,
            'from_account' => $payment->from_account,
            'to_account' => $payment->to_account,
            'message' => $payment->message,
            'amount' => $payment->amount,
            'value' => (int)$payment->amount

        ];

    }

    /**
     * @param $flat
     * @return array
     */
    public static function getFlatPayments($flat)
    {
        $out = [];


        foreach ($flat->contracts as $contract) {
            foreach ($contract->payments as $k => $payment) {
                //$out[$payment->id]['parent'] = $payment->parent_payment;

                if (!empty($payment->parent_payment)) {

                    $out[$payment->parent_payment]['partial'][$k] = self::_paymentDetail($payment);

                    if (isset($out[$payment->parent_payment]['total'])) {
                        $out[$payment->parent_payment]['total'] += (int)$payment->amount;
                        //$out[$payment->parent_payment]['debug'][] = 'Total exists. Substracting '.$payment->amount .' from total';

                    } else {
                        $out[$payment->parent_payment]['total'] = (int)$payment->amount;
                        //$out[$payment->parent_payment]['debug'][] = 'Total DONT exists. Setting '.(int) $payment->amount .' as total';
                    }

                } else {

                    if (isset($out[$payment->id]['total'])) {
                        $out[$payment->id] = $out[$payment->id] + self::_paymentDetail($payment);
                    } else {
                        $out[$payment->id] = self::_paymentDetail($payment);
                    }

                    $out[$payment->id] = $out[$payment->id] + ['contract' => $contract->text];

                    if (isset($out[$payment->id]['total'])) {
                        $out[$payment->id]['total'] += (int)$payment->amount;
                        //$out[$payment->id]['debug'][] = 'Total exists. Adding '.$payment->amount .' to total';
                    } else {
                        $out[$payment->id]['total'] = (int)$payment->amount;
                        //$out[$payment->id]['debug'][] = 'Total DONT exists. Setting '.(int) $payment->amount .' as total';
                    }

                }


            }
        }

        //sort by date DESC
        usort($out, function ($a, $b) {
            return strtotime($b['created']) - strtotime($a['created']);
        });


        return $out;
    }

    /**
     * @param $flat
     * @return array|mixed
     */
    public static function getContractsWithPeople($flat)
    {

        $out = [];

        $contracts = $flat->contracts;
        foreach ($contracts as $contract) {
            $contractData = ArrayHelper::toArray($contract);
            $out[$contract->id] = $contractData;
            foreach ($contract->persons as $person) {
                $personData = ArrayHelper::toArray($person);
                $addressData = ArrayHelper::toArray($person->address);
                $out[$contract->id][$person->role_id][$person->id] = $personData + $addressData;
            }
        }

        $out = self::prepareContractItems($out);

        return $out;

    }


    /**
     * @param $contracts
     * @return mixed
     */
    public static function prepareContractItems($contracts)
    {
        $items = [
            'water_cold',
            'water_warm',
            'heat',
            'elevator',
            'electricity',
            'maintanance',
            'shared_electricity',
            'illumination',
            'cleaning'
        ];

        $model = new Contract;

        foreach ($contracts as $key => $contract) {
            foreach ($items as $item) {
                if (!empty($contract[$item])) {
                    $contracts[$key]['items'][$item]['name'] = $model->getAttributeLabel($item);
                    $contracts[$key]['items'][$item]['value'] = $contract[$item];
                }
            }
        }

        return $contracts;


    }

}


?>