<?php

use yii\db\Schema;
use yii\db\Migration;

class m150905_184736_flat extends Migration
{
    public function up()
    {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }     

      $this->createTable('flat', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'title'=>Schema::TYPE_STRING.'(255) NOT NULL',
         'description'=>Schema::TYPE_STRING.'(255) NULL',
         'street'=>Schema::TYPE_STRING.'(255) NOT NULL',
         'street_number'=>Schema::TYPE_STRING.'(100) NOT NULL',
         'city'=>Schema::TYPE_STRING.'(100) NOT NULL',
         'region'=>Schema::TYPE_STRING.'(100) NULL',
         ], $tableOptions);


    }

    public function down()
    {
         $this->dropTable('flat');
    }


  
}
