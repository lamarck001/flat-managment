<?php

use yii\db\Schema;
use yii\db\Migration;

class m150905_191724_tenant extends Migration
{
    public function up()
    {

      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }     

      $this->createTable('tenant', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'title'=>Schema::TYPE_STRING.'(10) NULL',
         'name'=>Schema::TYPE_STRING.'(255) NOT NULL',
         'surname'=>Schema::TYPE_STRING.'(255) NOT NULL',
         'birthday'=>Schema::TYPE_DATE.' NOT NULL',
         'sex'=>Schema::TYPE_STRING.'(10) NOT NULL',
         'id_type'=>Schema::TYPE_INTEGER.'(100) NOT NULL',
         'id_number'=>Schema::TYPE_STRING.'(100) NOT NULL',
         'flat_id'=>Schema::TYPE_INTEGER.' NOT NULL',
         'start_date'=>Schema::TYPE_DATE.' NOT NULL',
         'end_date'=>Schema::TYPE_DATE.' NULL',
         ], $tableOptions);

      $this->addForeignKey('fk_tennat_flat', 'tenant' , 'flat_id', 'flat', 'id');       

    }

    public function down()
    {
         $this->dropForeignKey('fk_tennat_flat', 'tenant'); 
         $this->dropTable('tenant');
    }
   
}
