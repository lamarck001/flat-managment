<?php

use yii\db\Schema;
use yii\db\Migration;

class m150906_162853_record extends Migration
{
    public function up()
    {

      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }     

      $this->createTable('record', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'type'=>Schema::TYPE_INTEGER.'(2) NOT NULL',
         'category'=>Schema::TYPE_INTEGER.' NOT NULL',
         'from'=>Schema::TYPE_DATE.' NOT NULL',
         'to'=>Schema::TYPE_DATE.' NOT NULL',
         'due'=>Schema::TYPE_DATE.' NULL',
         'done'=>Schema::TYPE_DATE.' NULL',
         'note'=>Schema::TYPE_STRING.'(255) NULL',
         'sum'=>Schema::TYPE_INTEGER.' NOT NULL',
         'flat_id'=>Schema::TYPE_INTEGER.' NOT NULL',
         'responsible'=>Schema::TYPE_INTEGER.'(2) NOT NULL',
         'status'=>Schema::TYPE_INTEGER.'(2) NULL',
         ], $tableOptions);

      $this->addForeignKey('fk_record_flat', 'record' , 'flat_id', 'flat', 'id');       
    }

    public function down()
    {
      $this->dropForeignKey('fk_record_flat', 'record'); 
      $this->dropTable('record');
    }

}
