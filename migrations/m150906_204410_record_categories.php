<?php

use yii\db\Schema;
use yii\db\Migration;

class m150906_204410_record_categories extends Migration
{
    public function up()
    { 
      
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }     
      
      $this->createTable('record_categories', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'title'=>Schema::TYPE_STRING.'(255) NOT NULL',
         ], $tableOptions);

      $this->addForeignKey('fk_record_categories', 'record' , 'category', 'record_categories', 'id');

      $this->batchInsert('record_categories', ['title'], [
            ['TUV'],
            ['společné osvětlení'],
            ['teplo'],
            ['fond oprav'],
            ['správa'],
            ['pojištení'],
            ['úklid'],
            ['výtah'],
            ['studená voda'],
            ['daň'],
            ['nájomné'],
            ['preplatok'],
            ['nedoplatok'],
            ['iné']
         ]
      );

    }

    public function down()
    {
      $this->dropForeignKey('fk_record_categories', 'record'); 
      $this->dropTable('record_categories');
    }

}
