<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_184223_document_templates extends Migration
{
    public function up()
    {
      
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }     
      
      $this->createTable('document_templates', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'title'=>Schema::TYPE_STRING.'(255) NOT NULL',
         'template'=> Schema::TYPE_TEXT.' NULL',
         ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('document_templates');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
