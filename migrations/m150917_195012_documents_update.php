<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_195012_documents_update extends Migration
{
    public function up()
    {
      $this->addColumn('documents', 'flat_id', Schema::TYPE_INTEGER.' NOT NULL');
      $this->addForeignKey('fk_document_flat_id', 'documents' , 'flat_id', 'flat', 'id');


      $this->addColumn('documents', 'tenant_id', Schema::TYPE_INTEGER.' NOT NULL');
      $this->addForeignKey('fk_document_tenant_id', 'documents' , 'tenant_id', 'tenant', 'id');

      $this->renameTable('documents','document');

    }

    public function down()
    {
      
      $this->renameTable('document','documents');
      $this->dropForeignKey('fk_document_flat_id', 'documents' );
      $this->dropColumn('documents', 'flat_id');
        
      $this->dropForeignKey('fk_document_tenant_id', 'documents' );
      $this->dropColumn('documents', 'tenant_id');
      




    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
