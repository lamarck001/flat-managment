<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_212527_documents_update2 extends Migration
{
    public function up()
    {
         $this->addColumn('document', 'created', Schema::TYPE_DATETIME.' NOT NULL DEFAULT CURRENT_TIMESTAMP');
         $this->addColumn('document', 'updated', Schema::TYPE_DATETIME.' NULL');
    }

    public function down()
    {
         $this->dropColumn('document', 'created');
         $this->dropColumn('document', 'updated');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
