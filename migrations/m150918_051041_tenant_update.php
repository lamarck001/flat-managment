<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_051041_tenant_update extends Migration
{
    public function up()
    {

      $this->addColumn('tenant', 'address_street', Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('tenant', 'address_street_number',  Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('tenant', 'address_city',  Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('tenant', 'address_zip',  Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('tenant', 'nationality',  Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('tenant', 'note',  Schema::TYPE_TEXT.' NULL');



    }

    public function down()
    {
        $this->dropColumn('tenant', 'address_street');
        $this->dropColumn('tenant', 'address_street_number');
        $this->dropColumn('tenant', 'address_city');
        $this->dropColumn('tenant', 'address_zip');
        $this->dropColumn('tenant', 'nationality');
        $this->dropColumn('tenant', 'note');

    }

    
}
