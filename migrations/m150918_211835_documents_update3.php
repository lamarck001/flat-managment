<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_211835_documents_update3 extends Migration
{
    public function up()
    {
         $this->addColumn('document', 'pdf', Schema::TYPE_STRING.' NULL');         
    }

    public function down()
    {
         $this->dropColumn('document', 'pdf');
    }
   
}
