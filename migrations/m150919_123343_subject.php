<?php

use yii\db\Schema;
use yii\db\Migration;

class m150919_123343_subject extends Migration
{
    public function up()
    {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql') {
         $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
      }    

      $this->createTable('subject', [
         'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
         'subject_type'=>Schema::TYPE_INTEGER.'(2) NOT NULL',
         'type'=>Schema::TYPE_INTEGER.'(2) NOT NULL',
         'company_name'=>Schema::TYPE_STRING.'(255) NULL',
         'address_street'=>Schema::TYPE_STRING.'(255) NULL',
         'address_street_number'=>Schema::TYPE_STRING.'(255) NULL',
         'address_city'=>Schema::TYPE_STRING.'(255) NULL',
         'address_zip'=>Schema::TYPE_STRING.'(255) NULL',
         'address_state'=>Schema::TYPE_STRING.'(255) NULL',
         'company_id'=>Schema::TYPE_STRING.'(255) NULL',
         'vat_id'=>Schema::TYPE_STRING.'(255) NULL',
         'web'=>Schema::TYPE_STRING.'(255) NULL',
         'phone'=>Schema::TYPE_STRING.'(255) NULL',
         'email'=>Schema::TYPE_STRING.'(255) NULL',
         'name'=>Schema::TYPE_STRING.'(255) NULL',
         'surname'=>Schema::TYPE_STRING.'(255) NULL',
         'date_of_birth'=>Schema::TYPE_DATE.' NULL',
         'sex'=>Schema::TYPE_STRING.'(10) NULL',
         'id_type'=>Schema::TYPE_INTEGER.'(100) NULL',
         'id_number'=>Schema::TYPE_STRING.'(100) NULL',
         'flat_id'=>Schema::TYPE_INTEGER.' NULL',
         'start_date'=>Schema::TYPE_DATE.' NULL',
         'end_date'=>Schema::TYPE_DATE.' NULL',
         'note'=>Schema::TYPE_STRING.'(255) NULL',
         'status'=>Schema::TYPE_INTEGER.'(2) NULL',
         ], $tableOptions); 

    }

    public function down()
    {
      $this->dropTable('subject');
    }

}
