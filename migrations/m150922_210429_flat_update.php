<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_210429_flat_update extends Migration
{
    public function up()
    {
         $this->addColumn('flat', 'account', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
         $this->addForeignKey('fk_flat_account_id', 'flat' , 'account', 'user', 'id');      
    }

    public function down()
    {
         
         $this->dropForeignKey('fk_flat_account_id', 'flat');
         $this->dropColumn('flat', 'account'); 
    }
}
