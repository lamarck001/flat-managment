<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_072043_flat_update2 extends Migration
{
   public function up()
   {
      $this->addColumn('flat', 'created_at', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1442945907');
      $this->addColumn('flat', 'updated_at', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1442945907');
      $this->addColumn('flat', 'updated_by', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');

      $this->addColumn('flat', 'zip', Schema::TYPE_STRING.'(255) NULL');
      $this->addColumn('flat', 'state', Schema::TYPE_STRING.'(255) NULL'); 

      $this->addColumn('flat', 'purchase_price', Schema::TYPE_INTEGER.' NULL');
      $this->addColumn('flat', 'current_price', Schema::TYPE_INTEGER.' NULL');

   }

   public function down()
   {
      $this->dropColumn('flat', 'created_at');
      $this->dropColumn('flat', 'updated_at');
      $this->dropColumn('flat', 'updated_by');

      $this->dropColumn('flat', 'zip');
      $this->dropColumn('flat', 'state');

      $this->dropColumn('flat', 'purchase_price');
      $this->dropColumn('flat', 'current_price');
   }
}
