<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_204548_subject_update extends Migration
{
    public function up()
    {
         $this->addColumn('subject', 'user_id', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
         $this->addForeignKey('fk_subject_user_id', 'subject' , 'user_id', 'user', 'id'); 
    }

    public function down()
    {
         $this->dropForeignKey('fk_subject_user_id', 'subject');
         $this->dropColumn('subject', 'user_id'); 
    }

   
}
