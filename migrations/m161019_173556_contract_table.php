<?php

use yii\db\Migration;
use yii\db\Schema;

class m161019_173556_contract_table extends Migration
{
    public function safeUp()
    {   
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        }

        $this->createTable('contract', [    
            'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'text'=>Schema::TYPE_TEXT.'(255) NULL',
            'from'=>Schema::TYPE_DATE.' NULL',
            'to'=>Schema::TYPE_DATE.' NULL',
           ], $tableOptions
        );


        $this->batchInsert('contract', ['text'], [
              ['Najomna zmluva'],
              ['Zmluva - elektrina'],
              ['Zmluva - poplatky']              
           ]
        );

    }

    public function safeDown()
    {
        $this->dropTable('contract');
    }

}
