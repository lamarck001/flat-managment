<?php

use yii\db\Migration;
use yii\db\Schema;

class m161019_174140_payment_tables extends Migration
{
    public function safeUp()
    {   

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        } 

        $this->createTable('payment', [
           'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
           'type'=>Schema::TYPE_INTEGER.' NOT NULL',
           'text'=>Schema::TYPE_TEXT.'(255) NULL',
           'from'=>Schema::TYPE_DATE.' NULL',
           'to'=>Schema::TYPE_DATE.'  NULL',
           'due'=>Schema::TYPE_DATE.' NULL',
           'created'=>Schema::TYPE_DATE.' NULL',
           'finished'=>Schema::TYPE_DATE.' NULL',
           'note'=>Schema::TYPE_STRING.'(255) NULL',
           ], $tableOptions
        );

        $this->batchInsert('payment', ['type','text'], [
              [1,'Najomne januar 2016'],
              [1,'Najomne februar 2016'],
              [1,'Najomne marec 2016']
           ]
        );


        $this->createTable('contract_payment', [
           'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'payment_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ]
        );

        $this->addPrimaryKey('contract_payment_pk', 'contract_payment', ['contract_id', 'payment_id']);
        $this->addForeignKey('fk_contract_payment_contract', 'contract_payment' , 'contract_id', 'contract', 'id'); 
        $this->addForeignKey('fk_contract_payment_payment', 'contract_payment' , 'payment_id', 'payment', 'id');

        $this->batchInsert('contract_payment', ['contract_id','payment_id'], [
              [1,1],
              [1,2],
              [1,3]              
           ]
        );


    }

    public function safeDown()
    {
        $this->dropTable('payment');

        $this->dropForeignKey('fk_contract_payment_contract', 'contract_payment'); 
        $this->dropForeignKey('fk_contract_payment_contract', 'contract_payment'); 
        $this->dropTable('contract_payment');
    }

}
