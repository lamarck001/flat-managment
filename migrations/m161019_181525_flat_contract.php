<?php

use yii\db\Migration;
use yii\db\Schema;

class m161019_181525_flat_contract extends Migration
{
    public function safeUp()
    {   

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        } 


        $this->createTable('flat_contract', [
           'flat_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ],$tableOptions
        );

        $this->addPrimaryKey('pk_flat_contract', 'flat_contract', ['flat_id', 'contract_id']);        
        $this->addForeignKey('fk_flat_contract_contract', 'flat_contract' , 'flat_id', 'flat', 'id'); 
        $this->addForeignKey('fk_flat_contract_flat', 'flat_contract' , 'contract_id', 'contract', 'id');

        $this->batchInsert('flat_contract', ['flat_id','contract_id'], [
              [1,1]
           ]
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_flat_contract_contract', 'flat_contract'); 
        $this->dropForeignKey('fk_flat_contract_flat', 'flat_contract'); 
        $this->dropTable('flat_contract');
    }


}
