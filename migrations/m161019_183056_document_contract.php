<?php

use yii\db\Migration;
use yii\db\Schema;


class m161019_183056_document_contract extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        } 


        //$this->dropForeignKey('fk_document_flat_id', 'document'); 
        //$this->dropForeignKey('fk_document_tenant_id', 'document'); 

        $this->createTable('document_contract', [
           'document_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ],$tableOptions
        );

        $this->addPrimaryKey('pk_document_contract', 'document_contract', ['document_id', 'contract_id']);        
        $this->addForeignKey('fk_document_contract_contract', 'document_contract' , 'contract_id', 'contract', 'id'); 
        $this->addForeignKey('fk_document_contract_document', 'document_contract' , 'document_id', 'document', 'id');

        $this->batchInsert('document_contract', ['document_id','contract_id'], [
              [103,1],
              [105,1]
           ]
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_document_contract_contract', 'document_contract'); 
        $this->dropForeignKey('fk_document_contract_document', 'document_contract'); 
        $this->dropTable('document_contract');

        //$this->addForeignKey('fk_document_flat_id', 'documents' , 'flat_id', 'flat', 'id');
        //$this->addForeignKey('fk_document_tenant_id', 'documents' , 'tenant_id', 'tenant', 'id');

    }

}
