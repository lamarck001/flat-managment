<?php

use yii\db\Migration;
use yii\db\Schema;

class m161020_174947_flat_record extends Migration
{
    public function safeUp()
    {   

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        } 


        $this->createTable('flat_record', [
           'flat_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'record_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ],$tableOptions
        );

        $this->addPrimaryKey('pk_flat_record', 'flat_record', ['flat_id', 'record_id']);        
        $this->addForeignKey('fk_flat_record_flat', 'flat_record' , 'flat_id', 'flat', 'id'); 
        $this->addForeignKey('fk_flat_record_record', 'flat_record' , 'record_id', 'record', 'id');
        
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_flat_record_record', 'flat_record'); 
        $this->dropForeignKey('fk_flat_record_flat', 'flat_record'); 
        $this->dropTable('flat_record');
    }
}
