<?php

use yii\db\Migration;
use yii\db\Schema;

class m161020_183410_person extends Migration
{
  public function safeUp()
  {   

    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
    } 

    $this->createTable('address', [     
            'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'street' =>Schema::TYPE_STRING.'(255) NULL',
            'street_number' =>Schema::TYPE_STRING.'(255) NULL',
            'street_number2' =>Schema::TYPE_STRING.'(255) NULL',
            'city' =>Schema::TYPE_STRING.'(255) NULL',
            'zip' =>Schema::TYPE_STRING.'(255) NULL',
            'state' =>Schema::TYPE_STRING.'(255) NULL',
            'note' =>Schema::TYPE_STRING.'(255) NULL'
        ], $tableOptions
    );    

    $this->createTable('person', [ 
        'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
        'address_id' =>Schema::TYPE_INTEGER.' NULL',
        'created'=>Schema::TYPE_DATE.' NOT NULL',
        'title_before' =>Schema::TYPE_STRING.'(255) NULL',
        'title_after' =>Schema::TYPE_STRING.'(255) NULL',
        'name' =>Schema::TYPE_STRING.'(255) NOT NULL',
        'middle_surname' =>Schema::TYPE_STRING.'(255) NULL',
        'surname' =>Schema::TYPE_STRING.'(255) NOT NULL',
        'birthday' =>Schema::TYPE_STRING.'(255) NULL',
        'nationality' =>Schema::TYPE_STRING.'(255) NULL',
        'sex' =>Schema::TYPE_STRING.'(255) NULL',
        'id_number' =>Schema::TYPE_STRING.'(255) NULL',
        'passport_number' =>Schema::TYPE_STRING.'(255) NULL',
        'phone_number' =>Schema::TYPE_STRING.'(255) NULL',
        'mobile_phone_number' =>Schema::TYPE_STRING.'(255) NULL',
        'email' =>Schema::TYPE_STRING.'(255) NULL',
        'note' =>Schema::TYPE_STRING.'(255) NULL'
        ], $tableOptions
    );

    $this->addForeignKey('fk_person_address', 'person' , 'address_id', 'address', 'id'); 


    $this->createTable('contract_person', [
       'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',
       'person_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
       ],$tableOptions
    );

    $this->addPrimaryKey('pk_contract_person', 'contract_person', ['contract_id', 'person_id']);        
    $this->addForeignKey('fk_contract_person_person', 'contract_person' , 'person_id', 'person', 'id'); 
    $this->addForeignKey('fk_contract_person_contract', 'contract_person' , 'contract_id', 'contract', 'id');
      
  }

    public function safeDown()
    {
        $this->dropForeignKey('fk_contract_person_person', 'contract_person'); 
        $this->dropForeignKey('fk_contract_person_contract', 'contract_person'); 
        $this->dropTable('contract_person');

        $this->dropForeignKey('fk_person_address', 'person');
        $this->dropTable('person');

        $this->dropTable('address');
    }
}
