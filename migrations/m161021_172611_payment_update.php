<?php

use yii\db\Migration;
use yii\db\schema;

class m161021_172611_payment_update extends Migration
{
    public function safeUp()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        }

        $this->dropForeignKey('fk_contract_payment_contract', 'contract_payment'); 
        $this->dropForeignKey('fk_contract_payment_contract', 'contract_payment'); 
        $this->dropTable('contract_payment');

        $this->dropForeignKey('fk_document_contract_contract', 'document_contract'); 
        $this->dropForeignKey('fk_document_contract_document', 'document_contract'); 
        $this->dropTable('document_contract');

        $this->dropForeignKey('fk_flat_record_record', 'flat_record'); 
        $this->dropForeignKey('fk_flat_record_flat', 'flat_record'); 
        $this->dropTable('flat_record');

        $this->addColumn('payment', 'contract_id', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
        $this->addColumn('payment', 'flat_id', Schema::TYPE_INTEGER.' NOT NULL ');
        $this->addColumn('payment', 'variable_symbol', Schema::TYPE_STRING.'(255) NULL');        
        $this->addColumn('payment', 'method', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
        $this->addColumn('payment', 'type2', Schema::TYPE_INTEGER.' NULL DEFAULT 1');                
        $this->addColumn('payment', 'from_account', Schema::TYPE_STRING.'(255) NULL');        
        $this->addColumn('payment', 'to_account', Schema::TYPE_STRING.'(255) NULL');
        $this->addColumn('payment', 'message', Schema::TYPE_STRING.'(255) NULL');

        $this->addForeignKey('fk_contract_payment', 'payment' , 'contract_id', 'contract', 'id'); 

        $this->addColumn('address', 'flat_number', Schema::TYPE_STRING.'(20) NULL');   


        $this->dropForeignKey('fk_record_categories', 'record');
        $this->dropColumn('record', 'category');
        $this->dropColumn('record', 'from');
        $this->dropColumn('record', 'to');
        $this->dropColumn('record', 'done');
        $this->dropColumn('record', 'note');
        $this->dropColumn('record', 'sum');
        $this->dropColumn('record', 'responsible');


        $this->addColumn('record', 'created', Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT CURRENT_TIMESTAMP' ); 
        $this->addColumn('record', 'updated', Schema::TYPE_DATE.' NULL'); 
        $this->addColumn('record', 'finished', Schema::TYPE_DATE.' NULL'); 
        $this->addColumn('record', 'created_by', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1'); 
        $this->addColumn('record', 'updated_by', Schema::TYPE_INTEGER.' NULL'); 
        $this->addColumn('record', 'assigned_to', Schema::TYPE_INTEGER.' NULL'); 
        $this->addColumn('record', 'resolved_by', Schema::TYPE_INTEGER.' NULL'); 
        $this->addColumn('record', 'title', Schema::TYPE_STRING.'(255) NULL'); 
        $this->addColumn('record', 'text', Schema::TYPE_TEXT.' NULL'); 
        $this->addColumn('record', 'resolution', Schema::TYPE_INTEGER.' NULL'); 
        $this->addColumn('record', 'priority', Schema::TYPE_INTEGER.' NULL'); 

        $this->addForeignKey('fk_record_created_by', 'record' , 'created_by', 'user', 'id'); 
        $this->addForeignKey('fk_record_updated_by', 'record' , 'updated_by', 'user', 'id'); 
        $this->addForeignKey('fk_record_assigned_to', 'record' , 'assigned_to', 'user', 'id'); 
        $this->addForeignKey('fk_record_resolved_by', 'record' , 'resolved_by', 'user', 'id');

        $this->dropTable('record_categories');


    }

    public function safeDown()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        } 

        $this->createTable('contract_payment', [
           'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'payment_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ]
        );

        $this->addPrimaryKey('contract_payment_pk', 'contract_payment', ['contract_id', 'payment_id']);
        $this->addForeignKey('fk_contract_payment_contract', 'contract_payment' , 'contract_id', 'contract', 'id'); 
        $this->addForeignKey('fk_contract_payment_payment', 'contract_payment' , 'payment_id', 'payment', 'id');

        $this->createTable('document_contract', [
           'document_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'contract_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ],$tableOptions
        );

        $this->addPrimaryKey('pk_document_contract', 'document_contract', ['document_id', 'contract_id']);        
        $this->addForeignKey('fk_document_contract_contract', 'document_contract' , 'contract_id', 'contract', 'id'); 
        $this->addForeignKey('fk_document_contract_document', 'document_contract' , 'document_id', 'document', 'id');


        $this->createTable('flat_record', [
           'flat_id'=>Schema::TYPE_INTEGER.' NOT NULL',
           'record_id'=>Schema::TYPE_INTEGER.' NOT NULL',           
           ],$tableOptions
        );

        $this->addPrimaryKey('pk_flat_record', 'flat_record', ['flat_id', 'record_id']);        
        $this->addForeignKey('fk_flat_record_flat', 'flat_record' , 'flat_id', 'flat', 'id'); 
        $this->addForeignKey('fk_flat_record_record', 'flat_record' , 'record_id', 'record', 'id');



        $this->dropForeignKey('fk_contract_payment', 'payment');   

        $this->dropColumn('payment', 'contract_id');
        $this->dropColumn('payment', 'flat_id');
        $this->dropColumn('payment', 'method');
        $this->dropColumn('payment', 'type2');
        $this->dropColumn('payment', 'variable_symbol');
        $this->dropColumn('payment', 'from_account');
        $this->dropColumn('payment', 'to_account');
        $this->dropColumn('payment', 'message');        

        $this->dropColumn('address', 'flat_number');



        $this->addColumn('record','category', Schema::TYPE_INTEGER.' NOT NULL');
        $this->addColumn('record','from', Schema::TYPE_DATE.' NOT NULL');
        $this->addColumn('record','to', Schema::TYPE_DATE.' NOT NULL');
        $this->addColumn('record','done', Schema::TYPE_DATE.' NULL');
        $this->addColumn('record','note', Schema::TYPE_STRING.'(255) NULL');
        $this->addColumn('record','sum', Schema::TYPE_INTEGER.' NOT NULL');
        $this->addColumn('record','responsible', Schema::TYPE_INTEGER.'(2) NOT NULL');

        $this->addForeignKey('fk_record_categories', 'record' , 'flat_id', 'flat', 'id'); 


        $this->dropForeignKey('fk_record_created_by', 'record');
        $this->dropForeignKey('fk_record_updated_by', 'record');
        $this->dropForeignKey('fk_record_assigned_to', 'record');
        $this->dropForeignKey('fk_record_resolved_by', 'record');

        $this->dropColumn('record', 'created'); 
        $this->dropColumn('record', 'updated'); 
        $this->dropColumn('record', 'finished'); 
        $this->dropColumn('record', 'created_by'); 
        $this->dropColumn('record', 'updated_by'); 
        $this->dropColumn('record', 'assigned_to'); 
        $this->dropColumn('record', 'resolved_by'); 
        $this->dropColumn('record', 'title'); 
        $this->dropColumn('record', 'text'); 
        $this->dropColumn('record', 'resolution'); 
        $this->dropColumn('record', 'priority'); 

        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
           $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        }     
        
        $this->createTable('record_categories', [
           'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
           'title'=>Schema::TYPE_STRING.'(255) NOT NULL',
           ], $tableOptions);

        

        $this->batchInsert('record_categories', ['title'], [
              ['TUV'],
              ['společné osvětlení'],
              ['teplo'],
              ['fond oprav'],
              ['správa'],
              ['pojištení'],
              ['úklid'],
              ['výtah'],
              ['studená voda'],
              ['daň'],
              ['nájomné'],
              ['preplatok'],
              ['nedoplatok'],
              ['iné']
           ]
        );


        
    }


}
