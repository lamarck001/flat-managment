<?php

use yii\db\Migration;
use yii\db\Schema;

class m161021_195816_contract_update extends Migration
{
    public function safeUp()
    {           
        $this->addColumn('contract', 'title', Schema::TYPE_STRING.'(255) NULL'); 
        $this->addColumn('contract', 'type', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
        $this->addColumn('contract', 'flat_id', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');        
        $this->addColumn('contract', 'total_amount', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'water_cold', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'water_warm', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'heat', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'elevator', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'electricity', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'maintanance', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'shared_electricity', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'illumination', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'cleaning', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'down_payment', Schema::TYPE_INTEGER.' NULL ');
        $this->addColumn('contract', 'payment_due', Schema::TYPE_DATE.' NULL');
        $this->addColumn('contract', 'electricity_due', Schema::TYPE_DATE.' NULL');
        $this->addColumn('contract', 'advances_due', Schema::TYPE_DATE.' NULL');

        $this->addForeignKey('fk_contract_flat', 'contract' , 'flat_id', 'flat', 'id'); 

    }   

    public function safeDown()
    {   
        
        $this->dropForeignKey('fk_contract_flat', 'contract'); 

        $this->dropColumn('contract', 'title');
        $this->dropColumn('contract', 'type');
        $this->dropColumn('contract', 'flat_id');
        $this->dropColumn('contract', 'total_amount');
        $this->dropColumn('contract', 'water_cold');
        $this->dropColumn('contract', 'water_warm');
        $this->dropColumn('contract', 'heat');
        $this->dropColumn('contract', 'elevator');
        $this->dropColumn('contract', 'electricity');
        $this->dropColumn('contract', 'maintanance');
        $this->dropColumn('contract', 'shared_electricity');
        $this->dropColumn('contract', 'illumination');
        $this->dropColumn('contract', 'cleaning');
        $this->dropColumn('contract', 'down_payment');
        $this->dropColumn('contract', 'payment_due');
        $this->dropColumn('contract', 'electricity_due');
        $this->dropColumn('contract', 'advances_due');
    }


}
