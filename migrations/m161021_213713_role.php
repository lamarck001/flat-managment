<?php

use yii\db\Migration;
use yii\db\Schema;

class m161021_213713_role extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
           $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';       
        }    

        $this->createTable('role', [
           'id'=>Schema::TYPE_INTEGER.' NOT NULL AUTO_INCREMENT PRIMARY KEY',
           'title'=>Schema::TYPE_STRING.'(100) NOT NULL'
           ],$tableOptions
        );
        
        $this->batchInsert('role', ['id','title'], [
              [1,'nobody']
           ]
        );

        $this->addColumn('person', 'role_id', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1');
        $this->addForeignKey('fk_person_role', 'person' , 'role_id', 'role', 'id'); 

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_person_role', 'person');
        $this->dropColumn('person', 'role_id');

        $this->dropTable('role');
    }

}
