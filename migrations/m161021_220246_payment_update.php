<?php

use yii\db\Migration;
use yii\db\Schema;

class m161021_220246_payment_update extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment', 'amount', Schema::TYPE_INTEGER.' NULL ');
    }

    public function safeDown()
    {
      $this->dropColumn('payment','amount');
    }

}
