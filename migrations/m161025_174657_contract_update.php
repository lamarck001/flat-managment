<?php

use yii\db\Migration;
use yii\db\Schema;


class m161025_174657_contract_update extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contract', 'is_active', Schema::TYPE_INTEGER.' NULL DEFAULT 0');
    }

    public function safeDown()
    {
      $this->dropColumn('contract','is_active');
    }
}
