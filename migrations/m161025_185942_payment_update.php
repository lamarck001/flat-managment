<?php

use yii\db\Migration;
use yii\db\Schema;

class m161025_185942_payment_update extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment', 'parent_payment', Schema::TYPE_INTEGER.' NULL ');
    }

    public function safeDown()
    {
        $this->dropColumn('payment','parent_payment');
    }
}
