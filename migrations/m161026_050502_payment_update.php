<?php

use yii\db\Migration;
use yii\db\Schema;

class m161026_050502_payment_update extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment', 'created', Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT CURRENT_TIMESTAMP' );
        $this->alterColumn('payment', 'from', Schema::TYPE_TIMESTAMP.'  NULL ' ); 
        $this->alterColumn('payment', 'to', Schema::TYPE_TIMESTAMP.'  NULL ' ); 
        $this->alterColumn('payment', 'due', Schema::TYPE_TIMESTAMP.'  NULL ' );
        $this->alterColumn('payment', 'finished', Schema::TYPE_TIMESTAMP.'  NULL ' ); 
    }

    public function safeDown()
    {
       
    }
}
