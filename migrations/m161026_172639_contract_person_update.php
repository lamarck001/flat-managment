<?php

use yii\db\Migration;
use yii\db\Schema;

class m161026_172639_contract_person_update extends Migration
{
    public function safeUp()
    {        
       
        $this->dropForeignKey('fk_person_role', 'person');
        
        $this->addColumn('contract_person', 'role_id', Schema::TYPE_INTEGER.' NOT NULL'); 
        $this->addForeignKey('fk_person_role', 'contract_person' , 'role_id', 'role', 'id'); 
    }

    public function safeDown()
    {   
        $this->dropForeignKey('fk_person_role', 'contract_person');
        $this->dropColumn('contract_person','role_id');

        $this->addForeignKey('fk_person_role', 'person' , 'role_id', 'role', 'id'); 
    }
}
