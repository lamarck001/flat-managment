<?php

use yii\db\Migration;
use yii\db\Schema;

class m161108_173055_payment_update extends Migration
{
    public function safeUp()
    {

        $this->addColumn('payment', 'period_code', Schema::TYPE_STRING . '(100) NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('payment', 'period_code');
    }
}
