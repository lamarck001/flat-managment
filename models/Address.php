<?php

namespace app\models; 

use Yii;


class Address extends \yii\db\ActiveRecord
{	

	public static function tableName()
	{
	    return 'address';
	}

	public function rules()
	{
	    return [
	        [['id'], 'required'],
	        [['street','street_number', 'street_number2', 'city'], 'string'],
	        [['street','street_number', 'street_number2', 'city'], 'safe'],
	    ];
	}

	public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),           
            'street' => Yii::t('app', 'Street'),
            'street_number' => Yii::t('app', 'Street_number'),
            'street_number2' => Yii::t('app', 'Street_number2')
        ];
    }
}
?>