<?php

namespace app\models; 

use Yii;


class Contract extends \yii\db\ActiveRecord
{	

	public static function tableName()
	{
	    return 'contract';
	}

	public function rules()
	{
	    return [
	        [['id'], 'required'],
	        [['id'], 'integer'],
	        [['text'], 'string'],
	        [['id', 'text','flat_id'], 'safe'],
	    ];
	}

	public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),           
            'text' => Yii::t('app', 'Name of the Contract') 
        ];
    }

	//Relations
	public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['contract_id' => 'id']);
    }

	public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['id' => 'document_id'])
            ->viaTable('document_contract', ['contract_id' => 'id']);
    }


	public function getFlat()
    {
        return $this->hasMany(Flat::className(), ['id' => 'flat_id'])
            ->viaTable('flat_contract', ['contract_id' => 'id']);
    }

    public function getPersonRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

	public function getPersons()
    {
        return $this->hasMany(Person::className(), ['id' => 'person_id'])
            ->viaTable('contract_person', ['contract_id' => 'id'])
            ->select('*, (SELECT role_id FROM contract_person WHERE contract_id='.$this->id.' AND person_id=person.id  LIMIT 1) as role_id');
    }






}
