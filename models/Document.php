<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

   public function behaviors()
   {
      return [
         [
            'class' => TimestampBehavior::className(),
            'attributes' => [
               ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
               ActiveRecord::EVENT_BEFORE_UPDATE => 'updated',
            ],
            'value' => new Expression('CURRENT_TIMESTAMP')
         ],
      ];
   }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','tenant_id','flat_id'], 'required'],
            [['text'],'string'],
            [['tenant_id','flat_id'], 'integer'],
            [['title'],'string', 'max' => 255],
            [['title','text','flat_id','tenant_id','pdf'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'flat_id' => Yii::t('app', 'Flat'),
            'tenant_id' => Yii::t('app', 'Tenant'),
            'pdf' => Yii::t('app', 'Pdf document'),
        ];
    }

   public function searchCurrentDocuments($flat_id)
   {
      $query = Document::find();

      $dataProvider = new ActiveDataProvider([
         'query' => $query,
      ]); 

      $query->andFilterWhere([
         'flat_id' => $flat_id,
      ]);

      return $dataProvider;
   }


   
}
