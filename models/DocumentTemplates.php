<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document_templates".
 *
 * @property integer $id
 * @property string $title
 * @property string $template
 */
class DocumentTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['template'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'template' => Yii::t('app', 'Template'),
        ];
    }
}
