<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DocumentTemplates;

/**
 * DocumentTemplatesSearch represents the model behind the search form about `app\models\DocumentTemplates`.
 */
class DocumentTemplatesSearch extends DocumentTemplates
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'template'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = DocumentTemplates::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'template', $this->template]);

        return $dataProvider;
    }
}
