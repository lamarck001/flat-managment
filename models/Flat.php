<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class Flat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'street', 'street_number', 'city', 'account'], 'required'],
            [['title', 'description', 'street'], 'string', 'max' => 255],
            [['street_number', 'city', 'region'], 'string', 'max' => 100],
            [['title', 'street', 'street_number', 'zip', 'state', 'description', 'region', 'account', 'purchase_price', 'current_price'], 'safe']
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['account', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_by',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->id;
                },
            ],

        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'street' => Yii::t('app', 'Street'),
            'street_number' => Yii::t('app', 'Street number'),
            'city' => Yii::t('app', 'City'),
            'region' => Yii::t('app', 'Region'),
            'account' => Yii::t('app', 'Account'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(Tenant::className(), ['flat_id' => 'id']);
    }

    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['flat_id' => 'id']);
    }


    public function getCurrentContracts()
    {
        return $this->hasMany(Contract::className(), ['flat_id' => 'id'])->where(['contract.is_active' => 1]);
    }

    public function getCurrentRentalContract()
    {
        return $this->hasMany(Contract::className(), ['flat_id' => 'id'])
            ->where(['AND', ['contract.is_active' => 1], ['contract.type' => 1]]);
    }


}


