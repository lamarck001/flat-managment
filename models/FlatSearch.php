<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Flat;

/**
 * FlatSearch represents the model behind the search form about `app\models\Flat`.
 */
class FlatSearch extends Flat
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'street', 'street_number', 'city', 'region'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    
   public function searchCurrent($params, $iud)
   {
      $query = Flat::find();

      $dataProvider = new ActiveDataProvider([
         'query' => $query,
      ]); 

      $query->andWhere([
         'account'=>$iud
      ]);


      if (!($this->load($params) && $this->validate())) {
        return $dataProvider;
      }

      $query->andFilterWhere([
         'id' => $this->id            
      ]);

      $query->andFilterWhere(['like', 'title', $this->title])
         ->andFilterWhere(['like', 'description', $this->description])
         ->andFilterWhere(['like', 'street', $this->street])
         ->andFilterWhere(['like', 'street_number', $this->street_number])
         ->andFilterWhere(['like', 'city', $this->city])
         ->andFilterWhere(['like', 'region', $this->region]);

      return $dataProvider;
   }


    public function search($params)
    {
        $query = Flat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]); 

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'street_number', $this->street_number])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'region', $this->region]);

        return $dataProvider;
    }
}
