<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;


class Payment extends ActiveRecord
{

    public static function tableName()
    {
        return 'payment';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'parent_payment',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'parent_payment',
                ],
                'value' => [$this, 'assignParentPayment']
            ],

        ];
    }

    public function rules()
    {
        return [
            [['type', 'amount', 'variable_symbol'], 'required'],
            [['id', 'type', 'amount', 'flat_id'], 'integer'],
            [['text', 'period_code'], 'string'],
            [['flat_id','finished', 'type2'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Payment type'),
            'text' => Yii::t('app', 'Text'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }

    public function assignParentPayment($event)
    {

        $vsPart = $event->sender->variable_symbol;
        $timePart = Yii::$app->formatter->asDate($event->sender->finished, 'MM_yyyy');
        $code = $vsPart.'_'.$timePart;

        if($event->sender->type2 == 2) {
            $event->sender->period_code = $code;
            $event->sender->amount = $event->sender->amount*-1;
            return;
        }

        $parentPayment = Payment::find()->where(['period_code'=>$code])->one();
        return $parentPayment->id;
    }

    public function getContract()
    {
        return $this->hasMany(Contract::className(), ['id' => 'contract_id'])
            ->viaTable('contract_payment', ['payment_id' => 'id']);
    }

}

?>