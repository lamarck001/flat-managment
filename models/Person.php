<?php

namespace app\models; 

use Yii;


class Person extends \yii\db\ActiveRecord
{


	public static function tableName()
	{
	    return 'person';
	}

	public function rules()
	{
	    return [
	        [['id','sex','name','surname'], 'required'],
	        [['id'], 'integer'],
            [['sex','name','surname'], 'safe'],
	    ];
	}

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_before' => Yii::t('app', 'Title before'),
            'title_after' => Yii::t('app', 'Title after'),
            'name' => Yii::t('app', 'Name'),
            'middle_surname' => Yii::t('app', 'Middle name'),
            'surname' => Yii::t('app', 'Surname'),
            'birthday' => Yii::t('app', 'Birthday'),
            'nationality' => Yii::t('app', 'Nationality'),
            'sex' => Yii::t('app', 'Sex'),            
            'id_number' => Yii::t('app', 'ID number'),
            'passport_number' => Yii::t('app', 'Passport number'),
            'phone_number' => Yii::t('app', 'Phone number'),
            'mobile_phone_number' => Yii::t('app', 'Mobile phone number'),
            'email' => Yii::t('app', 'Email'),
            'address_street' => Yii::t('app', 'Street'),
            'address_street_number' => Yii::t('app', 'Street number'),
            'address_street_number2' => Yii::t('app', 'Street number2'),
            'address_city' => Yii::t('app', 'City'),
            'address_zip' => Yii::t('app', 'ZIP'),            
            'address_state' => Yii::t('app', 'State'), 
            'note' => Yii::t('app', 'Note'),
        ];
    }


	//public function getRoles(){
     //   return $this->hasMany(Role::className(), ['id' => 'role_id']);
    	//return $this->hasMany(Role::className(), ['role_id' => 'id']);
    //}

    public function getAddress(){
       return $this->hasOne(Address::className(), ['id' => 'address_id']);
   }




}

?>