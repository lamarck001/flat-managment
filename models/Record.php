<?php

namespace app\models;
use app\models\RecordCategories;

use Yii;

class Record extends \yii\db\ActiveRecord
{
    
   public function getAllTypes() {
      return [
         '1'=>Yii::t('app', 'Payment'),
         '2'=>Yii::t('app', 'Payment rule'),
         '3'=>Yii::t('app', 'Phone contact'),
         '4'=>Yii::t('app', 'Personal contact'),
         '5'=>Yii::t('app', 'Sms contact'),
         '6'=>Yii::t('app', 'Mail contact'),
      ];
   }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type',  'created', 'flat_id', 'created_by', 'text'], 'required'],
            [['type', 'flat_id', 'created_by', 'status','created_by', 'priority'], 'integer'],
            //[['type', 'flat_id', 'created_by', 'due', 'finished', 'text'], 'safe'],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'finished' => Yii::t('app', 'Finished'),
            'note' => Yii::t('app', 'Note'),
            'sum' => Yii::t('app', 'Sum'),
            'flat_id' => Yii::t('app', 'Flat ID'),
            'created_by' => Yii::t('app', 'Created by'),
            'status' => Yii::t('app', 'Status'),
        ];
    }


    public function getFlat()
    {
        return $this->hasOne(Flat::className(), ['id' => 'flat_id']);
    }




}
