<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Record;

/**
 * RecordSearch represents the model behind the search form about `app\models\Record`.
 */
class RecordSearch extends Record
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'flat_id', 'created_by', 'status'], 'integer'],
            [['id','created_by', 'updated_by', 'resolved_by', 'resolved_by','created', 'updated', 'type','resolution', 'priority','due', 'finished', 'text','status', 'title'], 'safe'],
        ];
    }

   /**
   * @inheritdoc
   */
   public function scenarios()
   {
     // bypass scenarios() implementation in the parent class
     return Model::scenarios();
   }


   public function searchCurrent($params, $id)
   {
      $query = Record::find();

      $query->where([
          'flat_id' => $id,
          //'status'=> 1
      ]);

      $dataProvider = new ActiveDataProvider([
         'query' => $query,
      ]);

      $this->load($params);

      if (!$this->validate()) {
         // uncomment the following line if you do not want to return any records when validation fails
         // $query->where('0=1');
         return $dataProvider;
      }  


      /*$query->andFilterWhere([    
         'type' => $this->type,
         'due' => $this->due,
         'finished' => $this->finished,         
         'created_by' => $this->created_by,
         'status' => $this->status,
      ]);*/


      return $dataProvider;

   }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Record::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }  

         $query->andFilterWhere(['like', 'type', $this->type])
            //->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'sum', $this->sum]);
            /*->andFilterWhere(['like', 'street_number', $this->street_number])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'region', $this->region])*/
            
        /* $query->andFilterWhere([
             'id' => $this->id,
             'status'=> 1
         ]);*/
         
         /*
         $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'category' => $this->category,
            'from' => $this->from,
            'to' => $this->to,
            'due' => $this->due,
            'done' => $this->done,
            'sum' => $this->sum,
            'flat_id' => $this->flat_id,
            'responsible' => $this->responsible,
            'status' => $this->status,
        ]);
        */

        //$query->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }


}
