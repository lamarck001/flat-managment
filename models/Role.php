<?php
namespace app\models;
use Yii;


class Role extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
	    return 'role';
	}

	public function rules()
	{
	    return [
	        [['id','title'], 'required'],
	        [['id'], 'integer'],
	    ];
	}

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title')            
        ];
    }

}

?>