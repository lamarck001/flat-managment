<?php

namespace app\models;

use Yii;

class Subject extends \yii\db\ActiveRecord
{

   public function getSex() {
      return [
         '1'=>Yii::t('app', 'Male'),
         '2'=>Yii::t('app', 'Female'),
      ];
   }

   public function getSubjectTypes() {
      return [
         '1'=>Yii::t('app', 'Tenant'),
         '2'=>Yii::t('app', 'Owner'),
      ];
   }

   public function getIdType() {
      return [ 
         '1'=> Yii::t('app', 'ID card'),
         '2'=> Yii::t('app', 'Passport'),
         '3'=> Yii::t('app', 'Driver licence'),
      ];
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getAllFlats() 
   {
      
      return Flat::find()->asArray()->all();

      //return $this->hasOne(Flat::className(), ['id' => 'flat_id']);
   }
   


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','name', 'surname'], 'required'],
            [['type', 'id_type', 'flat_id', 'status'], 'integer'],
            [['date_of_birth', 'start_date', 'end_date','subject_type','company_name', 'address_street', 'address_street_number', 'address_city', 'address_zip', 'address_state', 'company_id', 'vat_id', 'web', 'phone', 'email', 'name', 'surname', 'note','sex','type','user_id'], 'safe'],
            [['company_name', 'address_street', 'address_street_number', 'address_city', 'address_zip', 'address_state', 'company_id', 'vat_id', 'web', 'phone', 'email', 'name', 'surname', 'note'], 'string', 'max' => 255],
            [['sex'], 'string', 'max' => 10],
            [['id_number'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'company_name' => Yii::t('app', 'Company Name'),
            'address_street' => Yii::t('app', 'Address Street'),
            'address_street_number' => Yii::t('app', 'Address Street Number'),
            'address_city' => Yii::t('app', 'Address City'),
            'address_zip' => Yii::t('app', 'Address Zip'),
            'address_state' => Yii::t('app', 'Address State'),
            'company_id' => Yii::t('app', 'Company ID'),
            'vat_id' => Yii::t('app', 'Vat ID'),
            'web' => Yii::t('app', 'Web'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'sex' => Yii::t('app', 'Sex'),
            'id_type' => Yii::t('app', 'Id Type'),
            'id_number' => Yii::t('app', 'Id Number'),
            'flat_id' => Yii::t('app', 'Flat ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
        ];
    }


    public function getRoles(){
      return $this->hasMany(Roles::className(), ['id' => 'role_id'])
            ->viaTable('subject_role', ['person_id' => 'id']);
    }

}
