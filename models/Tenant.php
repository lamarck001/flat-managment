<?php

namespace app\models;

use Yii;
use app\models\Flat;

/**
 * This is the model class for table "tenant".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $surname
 * @property string $birthday
 * @property string $sex
 * @property integer $id_type
 * @property string $id_number
 * @property integer $flat_id
 * @property string $start_date
 * @property string $end_date
 *
 * @property Flat $flat
 */ 
class Tenant extends \yii\db\ActiveRecord
{  

   public function getSex() {
      return [
         '1'=>Yii::t('app', 'Male'),
         '2'=>Yii::t('app', 'Female'),
      ];
   }

   public function getIdType() {
      return [ 
         '1'=> Yii::t('app', 'ID card'),
         '2'=> Yii::t('app', 'Passport'),
         '3'=> Yii::t('app', 'Driver licence'),
      ];
   }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'birthday', 'sex', 'id_type', 'id_number', 'flat_id', 'start_date'], 'required'],
            [['birthday', 'start_date', 'end_date','address_street','address_street_number','address_city','address_zip','nationality','note'], 'safe'],
            [['id_type', 'flat_id'], 'integer'],
            [['title', 'sex'], 'string', 'max' => 10],
            [['name', 'surname'], 'string', 'max' => 255],
            [['id_number'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'birthday' => Yii::t('app', 'Birthday'),
            'sex' => Yii::t('app', 'Sex'),
            'id_type' => Yii::t('app', 'ID type'),
            'id_number' => Yii::t('app', 'ID number'),
            'flat_id' => Yii::t('app', 'Flat'),
            'start_date' => Yii::t('app', 'Start date'),
            'end_date' => Yii::t('app', 'End date'),
            'address_street' => Yii::t('app', 'Street'),
            'address_street_number' => Yii::t('app', 'Street number'),
            'address_city' => Yii::t('app', 'City'),
            'address_zip' => Yii::t('app', 'ZIP'),
            'nationality' => Yii::t('app', 'Nationality'),
            'note' => Yii::t('app', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlat()
    {
        return $this->hasOne(Flat::className(), ['id' => 'flat_id']);
    }


   /**
    * @return \yii\db\ActiveQuery
    */
   public function getAllFlats()
   {
      
      return Flat::find()->asArray()->all();

      //return $this->hasOne(Flat::className(), ['id' => 'flat_id']);
   }
}
