<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tenant;

/**
 * TenantSearch represents the model behind the search form about `app\models\Tenant`.
 */
class TenantSearch extends Tenant
{
    public function rules()
    {
        return [
            [['id', 'id_type', 'flat_id'], 'integer'],
            [['title', 'name', 'surname', 'birthday', 'sex', 'id_number', 'start_date', 'end_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

   public function searchCurrent($params, $flat_id)
   {
       $query = Tenant::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
           // return $dataProvider;
        }

        $query->andFilterWhere([            
            'flat_id' => $flat_id,
            //'end_date' => $this->end_date,
        ]);

        /*$query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'id_number', $this->id_number]);*/

        return $dataProvider;
   }


    public function search($params)
    {
        $query = Subject::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subject_type'=> 2            
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'id_number', $this->id_number]);

        return $dataProvider;
    }
}
