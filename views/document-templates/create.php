<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\DocumentTemplates $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Document Templates',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Document Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-templates-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
