<?php
//use yii\helpers\Html;
use app\helpers\FlatManagementHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

//use app\models\Payment;
//use yii\helpers\ArrayHelper;

use kartik\icons\Icon;

Icon::map($this, Icon::FA);


$contracts = FlatManagementHelper::getContractsWithPeople($flat);


//echo '<pre>';print_r($contracts);echo '</pre>';


?>
<section class="contract-list">
    <div class="box">


    </div>


    <?php foreach ($contracts as $contract) { ?>
        <div>
            <div class="row">
                <div class="col-xs-1">
                    <?= $contract['is_active'] == 1 ? icon::show('check', ['class' => 'fa-3x'], Icon::FA) : icon::show('window-close', ['class' => 'fa-3x'], Icon::FA) ?>
                </div>
                <div class="col-xs-11">
                    <strong><?= $contract['text'] ?> |
                        <em><?= \Yii::$app->formatter->asDatetime($contract['from'], "php:d.m.Y"); ?> -
                            <?= \Yii::$app->formatter->asDatetime($contract['to'], "php:d.m.Y"); ?></em></strong>
                </div>

                <hr/>

                <div class="col-xs-6">

                    <?php

                    //echo '<pre>'; print_r($contract['people']); echo '</pre>';

                    ?>
                    <?php if (count($contract[2]) > 0) { ?>
                        <h4><?= count($contract[2]) > 1 ? Yii::t('app', 'Owners') : Yii::t('app', 'Owner') ?></h4>
                        <?php foreach ($contract[2] as $owner) { ?>
                            <h5><?= $owner['name'] . ' ' . $owner['surname'] ?></h5>
                            <p>
                                <?= $owner['street'] ?> <?= $owner['street_number'] ?> <?= $owner['street_number2'] ? '/' . $owner['street_number2'] : '' ?>
                            </p>
                        <?php } ?>
                    <?php } ?>

                    <?php if (count($contract[3]) > 0) { ?>
                        <h4><?= count($contract[3]) > 1 ? Yii::t('app', 'Tenants') : Yii::t('app', 'Tenant') ?></h4>
                        <?php foreach ($contract[3] as $tenant) { ?>
                            <h5><?= $tenant['name'] . ' ' . $tenant['surname'] ?></h5>
                            <p>
                                <?= $tenant['street'] ?> <?= $tenant['street_number'] ?> <?= $tenant['street_number2'] ? '/' . $tenant['street_number2'] : '' ?>
                            </p>
                        <?php } ?>
                    <?php } ?>

                    <?php if (count($contract[5]) > 0) { ?>
                        <h4><?= count($contract[5]) > 1 ? Yii::t('app', 'Companies') : Yii::t('app', 'Company') ?></h4>
                        <?php foreach ($contract[5] as $tenant) { ?>
                            <h5><?= $tenant['name'] . ' ' . $tenant['surname'] ?></h5>
                            <p>
                                <?= $tenant['street'] ?> <?= $tenant['street_number'] ?> <?= $tenant['street_number2'] ? '/' . $tenant['street_number2'] : '' ?>
                            </p>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="col-xs-6">

                    <table class="table table-striped">
                        <?php if (count($contract['items']) > 0) { ?>
                            <thead>
                            <tr>
                                <th><?= Yii::t('app', 'Item') ?></th>
                                <th><?= Yii::t('app', 'Amount') ?></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach ($contract['items'] as $key => $item) { ?>
                                <tr>
                                    <td><?= $item['name'] ?></td>
                                    <td><?= $item['value'] ?></td>
                                </tr>
                            <?php }
                            ?>
                            </tbody>
                        <?php } ?>
                        <tfoot>
                        <tr>
                            <th class="bg-primary"><?= Yii::t('app', 'Total') ?></th>
                            <th class="bg-primary"><?= $contract['total_amount'] ?></th>
                        </tr>
                        </tfoot>
                    </table>

                    <?php /* ?><p class="bg-primary text-right items-total"><?= Yii::t('app','Total') ?> <?= $contract['total_amount'] ?></p><?php */ ?>

                </div>

            </div>
        </div>
    <?php } ?>


</section>


