<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Flat $model
 * @var yii\widgets\ActiveForm $form
 */
?>


<?php 
// echo '<pre>'; print_r(Yii::$app->user->id); echo '</pre>';
// echo '<pre>'; print_r($model->account); echo '</pre>';

?>
<div class="flat-form">

   <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); 

      echo Form::widget([

         'model' => $model,
         'form' => $form,
         'columns' => 1,
         'attributes' => [
            'title'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Title...', 
                  'maxlength'=>255
               ]
            ], 
            'description'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Description...', 
                  'maxlength'=>255
               ]
            ], 
            'street'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Street...', 
                  'maxlength'=>255
               ]
            ], 
            'street_number'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Street Number...', 
                  'maxlength'=>100
               ]
            ], 
            'city'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter City...', 
                  'maxlength'=>100
               ]
            ], 
            'zip'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Zip', 
                  'maxlength'=>100
               ]
            ], 
            'state'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'State', 
                  'maxlength'=>100
               ]
            ],     
            'region'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Region...', 
                  'maxlength'=>100
               ]
            ],
            'purchase_price'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Region...', 
                  'maxlength'=>100
               ]
            ],
            'current_price'=>[
               'type'=> Form::INPUT_TEXT, 
               'options'=>[
                  'placeholder'=>'Enter Region...', 
                  'maxlength'=>100
               ]
            ],
         ]
      ]);

   
      echo $form->field($model, 'account')->hiddenInput(['value'=>Yii::$app->user->id])->label(false);

      echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);

   ActiveForm::end(); ?>

</div>
