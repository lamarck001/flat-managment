<?php
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use yii\bootstrap\Modal;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\widgets\Pjax;
use app\models\Record;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\View;
use kartik\icons\Icon;

Icon::map($this, Icon::FA);
use yii\redactor\widgets\Redactor;
use app\models\DocumentTemplates;
use app\models\Document;
use app\models\Subject;
use yii\helpers\Url;
use yii\widgets\ListView;

?>

<div class="row">
    <div class="col-xs-6">
        <?= DetailView::widget([
            'model' => $model,
            'condensed' => false,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'panel' => [
                'heading' => Yii::t('app', 'Flat details'),
            ],
            'formOptions' => [
                'action' => ['update', 'id' => $model->id, 'isAjax' => 1],
            ],
            'buttons1' => '{update}',
            'buttons2' => '{view}{reset}{save}',
            'attributes' => [
                [
                    'attribute' => 'title',
                    'value' => $model->title
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'street',
                            'label' => Yii::t('app', 'Address'),
                            'value' => $model->street . ' ' . $model->street_number,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Street name')
                            ],

                        ],
                        [
                            'attribute' => 'street_number',
                            'value' => $model->street_number,
                            'valueColOptions' => ['class' => 'hide-from-view'],
                            'labelColOptions' => ['style' => 'display:none'],
                            'options' => [
                                'placeholder' => Yii::t('app', 'street #')
                            ]
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'city',
                            'value' => $model->city . ' / ' . $model->zip,
                            'options' => [
                                'placeholder' => Yii::t('app', 'City')
                            ],

                        ],
                        [
                            'attribute' => 'zip',
                            'value' => $model->zip,
                            'valueColOptions' => ['class' => 'hide-from-view'],
                            'labelColOptions' => ['style' => 'display:none'],
                            'options' => [
                                'placeholder' => Yii::t('app', 'Zip')
                            ]
                        ],
                    ],
                ],

                [
                    'attribute' => 'state',
                    'format' => 'raw',
                    'value' => $model->state
                ],
                'description',
            ],
            'deleteOptions' => [
                'url' => [
                    'delete',
                    'id' => $model->id
                ],
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ],
            'enableEditMode' => true,
        ]) ?>


        <h4><?= Yii::t('app', 'Create document') ?></h4>

        <?php
        $templates = [0 => Yii::t('app', '- Choose document -')];
        $templates = $templates + ArrayHelper::map(DocumentTemplates::find()->all(), 'id', 'title');
        ?>

        <section class="documents">
            <div class="row">
                <div
                    class="col-xs-8"><?= Html::dropDownList('document-template', '', $templates, ['id' => 'doc', 'class' => 'form-control']) ?> </div>
                <div class="col-xs-4">
                    <?php Modal::begin([
                        'id' => 'create-document-modal',
                        'size' => 'modal-a4',
                        'header' => Html::a(Icon::show('save', [], Icon::FA) . 'Save document', ['#'], ['id' => 'document-save', 'class' => 'btn btn-success btn-sm save-doc']) .
                            Html::a(Icon::show('envelope', [], Icon::FA) . 'Save & send document', ['#'], ['id' => 'save-send', 'class' => 'btn btn-success btn-sm save-send-doc']) .
                            Html::a(Icon::show('file-pdf-o', [], Icon::FA) . 'Save & print to PDF', ['#'], ['id' => 'save-pdf', 'class' => 'btn btn-success btn-sm save-pdf-doc']),
                        'toggleButton' => [
                            'label' => Icon::show('file-text', [], Icon::FA) . ' Create',
                            'class' => 'btn btn-primary pull-right'
                        ]
                    ]);

                    //echo $form->field($model, 'contract_req_text_en')->label(false)->hiddenInput();

                    $form = ActiveForm::begin([
                        'id' => 'document-form'
                    ]);

                    echo $form->field($documentModel, 'title')->textInput(['id' => 'document-title']);

                    echo $form->field($documentModel, 'text')->widget(Redactor::className(), [
                        'options' => [
                            'id' => 'document-redactor'
                        ]
                    ])->label(false);

                    echo $form->field($documentModel, 'tenant_id')->hiddenInput(['value' => $tennatId])->label(false);
                    echo $form->field($documentModel, 'flat_id')->hiddenInput(['value' => $model->id])->label(false);
                    echo $form->field($documentModel, 'id')->hiddenInput()->label(false);


                    ActiveForm::end();

                    Modal::end(); ?>
                </div>
            </div>
        </section>

        <?php
        echo GridView::widget([
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'linkSelector' => false
                ]
            ],
            'id' => 'documents-list',
            'dataProvider' => $documentDataProvider,
            'columns' => [
                //'id',

                [
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function ($doc, $key, $index, $column) {
                        return Html::a(Icon::show('file-pdf-o', [], Icon::FA) . $doc->title, ['document/view', 'id' => $doc->id], ['class' => 'view', 'data-id' => $doc->id]);
                    }
                ],
                [
                    'attribute' => 'created',
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{edit}{delete}{download}',
                    'buttons' => [
                        'edit' => function ($url, $doc) {
                            return Html::a(Icon::show('pencil', [], Icon::FA), ['document/edit', 'id' => $doc->id], ['class' => 'edit', 'data-id' => $doc->id]);
                        },
                        'delete' => function ($url, $doc) {
                            return Html::a(Icon::show('times', [], Icon::FA), ['document/delete', 'id' => $doc->id]);
                        },
                        'download' => function ($url, $doc) {
                            return Html::a(Icon::show('download', [], Icon::FA), ['document/download', 'id' => $doc->id], ['class' => 'downloadLink']);
                        }
                    ]
                ]

            ],

            'condensed' => true,
            'panel' => false,
            'layout' => '{items}',
        ]);
        ?>


    </div>
    <div class="col-xs-6">
        <?php
        $people = $model->currentRentalContract[0]->persons;
        foreach ($people as $person) {
            if ($person[role_id] == 3) {
                echo DetailView::widget([
                    'model' => $person,
                    'condensed' => false,
                    'hover' => true,
                    'mode' => DetailView::MODE_VIEW,
                    'panel' => [
                        'heading' => Yii::t('app', 'Current tenant'),
                    ],
                    'formOptions' => [
                        'action' => ['person/update', 'id' => $person->id, 'isAjax' => 1],
                    ],
                    'buttons1' => '{update}',
                    'buttons2' => '{reset}{save}',
                    'attributes' => [
                        [
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'label' => Yii::t('app', 'Name'),
                                    'format' => 'raw',
                                    'value' => ($person->sex == 1 ? Icon::show('mars', [], Icon::FA) : Icon::show('venus', [], Icon::FA)) . $person->name . ' ' . $person->surname,
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'Name')
                                    ]
                                ],
                                [
                                    'attribute' => 'surname',
                                    'value' => $person->surname,
                                    'valueColOptions' => ['class' => 'hide-from-view'],
                                    'labelColOptions' => ['style' => 'display:none'],
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'surname')
                                    ]
                                ],
                            ],
                        ],
                        'birthday',
                        [
                            'attribute' => 'sex',
                            'value' => $person->sex == 1 ? Yii::t('app', 'Male') : Yii::t('app', 'Female')
                        ]
                    ],
                    'enableEditMode' => true,
                ]);
            }
        }

        ?>



        <?php /*
        Pjax::begin();
        if (isset($currentTenant)) {
            echo DetailView::widget([
                'model' => $currentTenant,
                'condensed' => false,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'panel' => [
                    'heading' => Yii::t('app', 'Current tenant'),
                ],
                'formOptions' => [
                    'action' => ['subject/update', 'id' => $currentTenant->id, 'isAjax' => 1],
                ],
                'buttons1' => '{update}',
                'buttons2' => '{reset}{save}',
                'attributes' => [
                    [
                        'columns' => [
                            [
                                'attribute' => 'name',
                                'label' => Yii::t('app', 'Name'),
                                'format' => 'raw',
                                'value' => ($currentTenant->sex == 1 ? Icon::show('mars', [], Icon::FA) : Icon::show('venus', [], Icon::FA)) . $currentTenant->name . ' ' . $currentTenant->surname,

                                'options' => [
                                    'placeholder' => Yii::t('app', 'Name')
                                ],

                            ],
                            [
                                'attribute' => 'surname',
                                'value' => $currentTenant->surname,
                                'valueColOptions' => ['class' => 'hide-from-view'],
                                'labelColOptions' => ['style' => 'display:none'],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'surname')
                                ]
                            ],
                        ],
                    ],


                    'date_of_birth',
                    [
                        'attribute' => 'sex',
                        'value' => $currentTenant->sex == 1 ? Yii::t('app', 'Male') : Yii::t('app', 'Female')
                    ],
                    [
                        'attribute' => 'id_type',
                        'label' => Yii::t('app', 'Identification'),
                        'value' => $currentTenant->getIdType()[$currentTenant->id_type] . ': ' . $currentTenant->id_number
                    ],
                    [
                        'attribute' => 'start_date',
                        'label' => Yii::t('app', 'Periode'),
                        'format' => 'raw',
                        'value' => 'Start: ' . $currentTenant->start_date . '<br /> End:' . $currentTenant->end_date
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'address_street',
                                'label' => Yii::t('app', 'Address'),
                                'value' => $currentTenant->address_street . ' ' . $currentTenant->address_street_number,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Street name')
                                ],

                            ],
                            [
                                'attribute' => 'address_street_number',
                                'value' => $currentTenant->address_street_number,
                                'valueColOptions' => ['class' => 'hide-from-view'],
                                'labelColOptions' => ['style' => 'display:none'],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'street #')
                                ]
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'address_city',
                                'label' => Yii::t('app', 'City'),
                                'value' => $currentTenant->address_city . ' / ' . $currentTenant->address_zip,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'City')
                                ],

                            ],
                            [
                                'attribute' => 'address_zip',
                                'value' => $currentTenant->address_zip,
                                'label' => Yii::t('app', 'Zip'),
                                'valueColOptions' => ['class' => 'hide-from-view'],
                                'labelColOptions' => ['style' => 'display:none'],
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Zip')
                                ]
                            ],
                        ],
                    ],

                    [
                        'attribute' => 'address_state',
                        'label' => Yii::t('app', 'State'),
                        'format' => 'raw',
                        'value' => $currentTenant->address_state
                    ],
                    'note',


                ],
                'enableEditMode' => true,
            ]);
        } else {
            ?>
            <h4>Please create tennat!</h4>

            <?php Modal::begin([
                'header' => '<h3>New Tenant</h3>',
                'toggleButton' => [
                    'label' => Icon::show('plus', [], Icon::FA) . Yii::t('app', 'New tenant'),
                    'class' => 'btn btn-primary'
                ],
            ]);

            //$record = new Record;
            //$recordModel = new Record;

            $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'id' => 'new-tenant-form',
                'action' => ['subject/create']
            ]);

            $newTenant = new Subject;
            echo Form::widget([

                'model' => $newTenant,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'name' => [
                        'type' => Form::INPUT_TEXT,
                    ],
                    'surname' => [
                        'type' => Form::INPUT_TEXT,
                    ]
                ]
            ]);

            echo $form->field($newTenant, 'subject_type')->hiddenInput(['value' => 1])->label(false);
            echo $form->field($newTenant, 'type')->hiddenInput(['value' => 1])->label(false);
            echo $form->field($newTenant, 'flat_id')->hiddenInput(['value' => $model->id])->label(false);
            echo $form->field($newTenant, 'account')->hiddenInput(['value' => Yii::$app->user->id])->label(false);

            echo $form->field($newTenant, 'id_type')->hiddenInput(['value' => 1])->label(false);
            echo $form->field($newTenant, 'sex')->hiddenInput(['value' => 1])->label(false);
            echo $form->field($newTenant, 'status')->hiddenInput(['value' => 1])->label(false);


            echo Html::submitButton(Yii::t('app', 'Save new tenant'), ['class' => 'btn btn-primary']);
            ActiveForm::end();
            Modal::end();
            ?>
            <?php
        }
        Pjax::end();
        */ ?>

    </div>
</div>

<section class="records">
    <div class="row">
        <div class="col-xs-12">

            <?php

            echo ListView::widget([
                'dataProvider' => $recordDataProvider,
                'itemView' => '_record_list',

            ]);


            Modal::begin([
                'header' => '<h3>' . Yii::t('app', 'New record') . '</h3>',
                'toggleButton' => [
                    'label' => Yii::t('app', 'New record'),
                    'class' => 'btn btn-primary'
                ],
            ]);


            $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL
            ]);

            echo Form::widget([

                'model' => $recordModel,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'text' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Enter Title...',
                            'maxlength' => 10
                        ]
                    ],
                    'due' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateControl::classname(),
                        'format' => ['date', 'php:d.m.Y'],
                        'options' => [
                            'type' => DateControl::FORMAT_DATE,
                        ]
                    ],
                    'flat_id' => [
                        //'class'=>'hidden',
                        'label' => false,
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'value' => $model->id,
                            'class' => 'hidden'
                        ]
                    ],
                    'assigned_to' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\widgets\Select2',
                        'options' => [
                            'data' => [
                                '1' => Yii::t('app', 'Owner'),
                                '2' => Yii::t('app', 'Tenant')
                            ]
                        ],
                    ],
                    'status' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\widgets\SwitchInput',
                        'options' => [
                            'value' => 1
                        ]
                    ]
                ]
            ]);
            echo Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']);
            ActiveForm::end();

            Modal::end();
            ?>

        </div>
    </div>
</section>