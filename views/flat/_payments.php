<?php
use yii\helpers\Html;
use app\helpers\FlatManagementHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use app\models\Payment;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use kartik\icons\Icon;
use yii\widgets\Pjax;
use kartik\datecontrol\DateControl;
use yii\web\View;

Icon::map($this, Icon::FA);

$payments = FlatManagementHelper::getFlatPayments($flat);

$currentContracts = $flat->currentContracts;

$currentContractsArray = ArrayHelper::toArray($currentContracts, [
    'app\models\Contract' => [
        'id',
        'text',
        'type'
    ],
]);


$js = <<< JAVASCRIPT

   $('.payment-detail').on('click', function(e) {
        $(this).siblings().slideToggle();
   });
   
   $('i[class="fa fa-info"]').on('click', function(e) {
        $(this).siblings('div').slideToggle();
   });

JAVASCRIPT;

$this->registerJs($js, View::POS_END);


?>
<section class="payment-list">
    <div class="box">
        <?php

        //Pjax::begin();

        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'id' => 'record-payment-form',
            'action' => ['payment/create']
        ]);

        $paymentModel->flat_id = $flat->id;
        $paymentModel->type = 1;
        $paymentModel->finished = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');


        foreach ($currentContractsArray as $contract) {
            if ($contract['type'] == 1) {
                $paymentModel->contract_id = $contract['id'];
            }
        }

        echo Form::widget([
            'model' => $paymentModel,
            'form' => $form,
            'columns' => 2,
            'attributes' => [

                'finished' => [
                    'label' => Yii::t('app', 'Date'),
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DateControl::classname(),
                    'options' => [
                        'type' => DateControl::FORMAT_DATE,
                    ]
                ],
                'amount' => [
                    'type' => Form::INPUT_TEXT
                ],
                'variable_symbol' => [
                    'type' => Form::INPUT_TEXT
                ],
                'type2' => [
                    'type' => Form::INPUT_WIDGET,
                    'label' => Yii::t('app', 'Payment type'),
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' => [
                        'data' => [
                            1 => Yii::t('app', 'Payment'),
                            2 => Yii::t('app', 'Payment order'),
                        ]
                    ]
                ],
                'contract_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'label' => Yii::t('app', 'Select contract'),
                    'widgetClass' => '\kartik\widgets\Select2',
                    'options' => [
                        'data' => ArrayHelper::map($currentContractsArray, 'id', 'text'),
                    ]
                ]
            ]
        ]);

        ?>
        <div>
            <span class="payment-detail btn btn-primary"><?= Yii::t('app', 'Show payment details') ?></span>
            <div style="display: none;">
                <?= Form::widget([
                    'model' => $paymentModel,
                    'form' => $form,
                    'columns' => 2,
                    'attributes' => [
                        'text' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Payment title')
                            ]
                        ],
                        'type' => [
                            'label' => false,
                            'type' => Form::INPUT_HIDDEN,
                        ],
                        'flat_id' => [
                            'label' => false,
                            'type' => Form::INPUT_HIDDEN,
                        ]
                    ]
                ]); ?>
            </div>
        </div>


        <?= Html::submitButton(Yii::t('app', 'Add payment'), ['class' => 'btn btn-primary']);


        ActiveForm::end();

        ?>
    </div>
    <?php foreach ($payments as $payment) { ?>
        <div>
            <div class="row">
                <div class="col-xs-2">
                    <h5><?= Yii::t('app', 'Due') ?></h5>
                    <?= \Yii::$app->formatter->asDatetime($payment['due'], "php:d.m.Y"); ?><br/>
                </div>
                <div class="col-xs-4">
                    <?php if ($payment['type2'] == 2) { ?>
                        <h5><?= Yii::t('app', 'Due payment') ?></h5>
                        <strong><?= $payment['amount'] ?> <?= Yii::$app->params['currency'] ?></strong> (<?= $payment['text'] ?>)
                    <?php } ?>
                </div>
                <div class="col-xs-4">
                    <?php if ($payment['type2'] == 1 && count($payment['partial']) < 1) { ?>
                        <h5><?= Yii::t('app', 'Paid') ?></h5>

                        <?= \Yii::$app->formatter->asDatetime($payment['created'], "php:d.m.Y"); ?>:
                        <strong><?= $payment['value'] ?> <?= Yii::$app->params['currency'] ?></strong>

                    <?php } else if (count($payment['partial']) > 0) { ?>
                        <h5><?= Yii::t('app', 'Paid') ?></h5>
                        <?php foreach ($payment['partial'] as $k => $partial_payment) { ?>
                            <div>
                                <strong><?= $partial_payment['value'] ?> <?= Yii::$app->params['currency'] ?></strong>
                                (<em><?= \Yii::$app->formatter->asDatetime($partial_payment['created'], "php:d.m.Y"); ?>
                                    ,<?= Yii::t('app', 'VS') ?>: <?= $partial_payment['variable_symbol'] ?></em>)
                                <?= Icon::show('info', [], Icon::FA) ?>
                                <div style="display: none; padding:2px 2px 2px 10px;">
                                    <strong><?= Yii::t('app', 'From account') ?>
                                        :</strong> <?= $partial_payment['from_account'] ?><br/>
                                    <strong><?= Yii::t('app', 'Variable symbol') ?>
                                        :</strong> <?= $partial_payment['variable_symbol'] ?><br/>
                                    <strong><?= Yii::t('app', 'Message') ?>:</strong> <?= $partial_payment['message'] ?>
                                    <br/>
                                    <strong><?= Yii::t('app', 'Note') ?>:</strong> <?= $partial_payment['note'] ?><br/>

                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col-xs-2">
                    <div class="<?= $payment['total'] > -1 ? 'text-success' : 'text-danger' ?>">
                        <?= $payment['total'] ?> <?= Yii::$app->params['currency'] ?>
                    </div>
                </div>
            </div>
        </div>
    <?php }
    //Pjax::end(); ?>

</section>


