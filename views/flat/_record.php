<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<p class="record">
    
   ID: <?= Html::encode($model->id) ?> |
   Type: <?= Html::encode($model->type) ?> | 
   Category: <?= Html::encode($model->category) ?> | 
   <?= Html::encode($model->note) ?>
</p>