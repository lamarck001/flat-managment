<?php 
use yii\helpers\Html;
?>

<div style="border:1px solid #ddd; margin: 10px 0; padding: 15px; background:#fcfcfc; border-radius: 3px;">

<h4><i><?= Html::encode($model->id) ?></i> - <?= Html::encode($model->title) ?> </h4><br />

<?= Html::encode($model->created_by) ?> |
<?= Html::encode($model->updated_by) ?> |
<?= Html::encode($model->resolved_by) ?> |
<?= Html::encode($model->resolved_by) ?> |
<?= Html::encode($model->created) ?> |
<?= Html::encode($model->updated) ?> |
<?= Html::encode($model->type) ?> |
<?= Html::encode($model->resolution) ?> |
<?= Html::encode($model->priority) ?> |
<?= Html::encode($model->due) ?> |
<?= Html::encode($model->finished) ?> |
<?= Html::encode($model->text) ?> |
<?= Html::encode($model->status) ?> |
 |

</div>