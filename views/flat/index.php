<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use kartik\icons\Icon;
Icon::map($this, Icon::FA);
Icon::map($this, Icon::EL);

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\FlatSearch $searchModel
 */

$this->title = 'Flats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flat-index">
     <?php Pjax::begin(); 

      echo GridView::widget([
           'dataProvider' => $dataProvider,
           'filterModel' => $searchModel,
           'columns' => [
               [
                  'class' => '\kartik\grid\ActionColumn',
                  'template'=>'{update}',
                  'updateOptions' => [
                     'label' => Icon::show('folder-open', [], Icon::FA),                     
                  ],
                  'urlCreator'=>function($action, $model, $key, $index) { 
                     //return '#';
                     return ['flat/view','id' => $model->id];
                  },                  
               ],                
               'title',               
               'street',
               'city',
               'region',
               'description',
            ],
            'responsive'=>true,
            'hover'=>true,
            'condensed'=>true,
            'floatHeader'=>true,
            'panel' => [
               'heading'=> Icon::show('home', [], Icon::FA).''.$this->title,
               //'type'=>'info',
               'before'=>Html::a(Icon::show('plus', [], Icon::BSG).'Add', ['create'], ['class' => 'btn btn-success']),
               'after'=>Html::a(Icon::show('repeat', [], Icon::BSG).'Reset List', ['index'], ['class' => 'btn btn-info']),
               'showFooter'=>false
            ],
      ]); 
   Pjax::end(); ?>

</div>
