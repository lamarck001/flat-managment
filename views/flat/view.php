<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use yii\bootstrap\Modal;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use yii\widgets\Pjax;
use app\models\Record;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\View;
use kartik\icons\Icon;

Icon::map($this, Icon::FA);

use yii\redactor\widgets\Redactor;
use app\models\DocumentTemplates;
use app\models\Document;
use app\models\Subject;
use yii\helpers\Url;
use yii\widgets\ListView;


use kartik\tabs\TabsX;


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Flats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$getDocTemplateUrl = Url::to(['document-templates/ajax-get-document']);

$saveDocUrl = Url::to(['document/save']);
$ajaxDelDocUrl = Url::to(['document/delete']);
$ajaxEditDocUrl = Url::to(['document/edit']);

new Redactor;

$tennatId = isset($currentTenant) ? $currentTenant->id : 1;

$js = <<< JAVASCRIPT

   $('#doc').on('change', function(e) {      
      var id = $(this).val(); 
      
      $('#document-form')[0].reset();
      $("#document-redactor").redactor('code.set', '');

      $.ajax({
         url: '$getDocTemplateUrl',
         data: {
            id: id,
            tenant_id: '$tennatId',
         },
         type: 'post',
         beforeSend: function() {
            $('.hide-me-spinner').show();
         }
      }).done(
         function(data) {
            $('#redactor').html(data.template);
            $('#document-title').val(data.title);
            $('#document-redactor').redactor('insert.set', data.template); 
         }
      );
   });

   $('.view').click(function(e) {
      e.preventDefault();      
      var id = $(this).data('id');

      $.ajax({
         url: '$ajaxEditDocUrl',
         data: {
            id: id
         },
         type: 'post',        
         beforeSend: function() {
            $('.hide-me-spinner').show();
         }
      }).done(
         function(data) {
            $('.modal-header a').hide();
            $('#redactor').html(data.text); 
            $('#document-redactor').redactor('insert.set', data.text); 
            $('#create-document-modal').modal('show');
         }
      ).fail(
         function(xhr, textStatus, errorThrown) {
            console.log('Error: ' + xhr.responseText);
         }
      );

   });

   $('.edit').click(function(e) {
      e.preventDefault();      
      var id = $(this).data('id');
      $.ajax({
         url: '$ajaxEditDocUrl',
         data: {
            id: id
         },
         type: 'post',        
         beforeSend: function() {
            $('.hide-me-spinner').show();
         }
      }).done(
         function(data) {
            $('#redactor').html(data.text);
            $('#document-title').val(data.title);
            $('#document-tenant_id').val(data.tenant_id);
            $('#document-flat_id').val(data.flat_id);
            $('#document-id').val(data.id);

            $('#document-redactor').redactor('insert.set', data.text);            


            $('#create-document-modal').modal('show');
            
            //console.log(d); 
         }
      ).fail(
         function(xhr, textStatus, errorThrown) {
            console.log('Error: ' + xhr.responseText);
         }
      );


   });

   $('#document-save').click(function(e) {
      e.preventDefault();              

      var form = $('#document-form');
      var formData = form.serialize();

      console.log(formData);

      $.ajax({
         url: '$saveDocUrl',
         data: formData,
         method: 'post',        
         beforeSend: function() {
            $('.hide-me-spinner').show();
         }
      }).done(
         function(data) {
            $('#create-document-modal').modal('hide');
            
           // $('#document-form')[0].reset();
            //$("#document-redactor").redactor('code.set', '');

            
            $.pjax({container: '#documents-list-pjax'});  

            

            
            //console.log($('#document-redactor')); 
         }
      ).fail(
         function() {
            console.log('Error');
         }
      );

   });

   /*function resetForm(){
      $('#document-form')[0].reset();
      $("#document-redactor").redactor('code.set', ''); 
   }*/

   //this is probably not necessary as 'linkSelector'=>false will solve this
   //$(document).pjax('#documents-list-pjax a:not(.no-data-pjax)', '#documents-list-pjax');

JAVASCRIPT;

$this->registerJs($js, View::POS_END);


$content1 = "Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim c";
$content2 = "Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR";
$content3 = 'sdsdsd';
$content4 = 'asc65a4c6a46c54a5';


$overview = $this->render('_overview', [
    'model' => $model,
    'documentModel' => $documentModel,
    'documentDataProvider' => $documentDataProvider,
    'recordDataProvider' => $recordDataProvider,
    'recordModel' => $recordModel
]);

$payments = $this->render('_payments', [
    'flat' => $model,
    'paymentModel'=>$paymentModel,
]);

$contracts = $this->render('_contracts', [
    'flat' => $model,
]);


$items = [
    [
        'label' => Icon::show('flash', [], Icon::FA) . Yii::t('app', 'Overview'),
        'content' => $overview,
        'active' => true
    ],
    [
        'label' => Icon::show('home', [], Icon::FA) . Yii::t('app', 'Property detail'),
        'content' => $content1,

    ],
    [
        'label' => Icon::show('money', [], Icon::FA) . Yii::t('app', 'Payments'),
        'content' => $payments,
        //'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/site/tabs-data'])]
    ],
    [
        'label' => Icon::show('file-text-o', [], Icon::FA) . Yii::t('app', 'Contracts'),
        'content' => $contracts,
    ],
    [
        'label' => Icon::show('file-text', [], Icon::FA) . Yii::t('app', 'Documents'),
        'content' => $content1,
    ],
];

?>


<div class="flat-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false,
        //'enableStickyTabs'=>true
    ]); ?>

</div>