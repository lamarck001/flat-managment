<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use kartik\nav\NavX;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

use kartik\icons\Icon;
Icon::map($this, Icon::FA);
use yii\helpers\Url;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


    <?php
    NavBar::begin([
        'brandLabel' => 'Flat management',
        
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo NavX::widget([
         'encodeLabels' => false,
         'options' => ['class' => 'navbar-nav navbar-right'],
         'items' => [
            //['label' => 'Home', 'url' => ['/site/index']],

            Yii::$app->user->can('accessUserAdmin') ? ['label' => 'Admin', 'url' => ['/admin']] : '',

            //Yii::$app->user->can('manageOwnFlats') ? ['label' => 'Flats', 'url' => ['/flat']] : '',
            
            //Yii::$app->user->can('manageOwnFlats') ? ['label' => 'Tenants', 'url' => ['/tenant']] : '',

            Yii::$app->user->isGuest ?
               ['label' => Icon::show('sign-in', [], Icon::FA).'Login', 'url' => ['/user/security/login']] :
               ['label' => Icon::show('sign-out', [], Icon::FA).Yii::t('app', 'Logout').' (' . Yii::$app->user->identity->username . ')',
                  'url' => ['/user/security/logout'],
                  'linkOptions' => ['data-method' => 'post']],
            
            ['label' => 'Register', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest]
        ],
    ]);
    NavBar::end();
    ?>



<div class="page-container container-fluid wrapper">
   <?php if(Yii::$app->user->can('manageOwnFlats')) { ?>
   <div class="sidebar collapse">
      <ul class="navigation">
         <li><a href="<?= Url::to(['/']); ?>"><?= Icon::show('laptop', [], Icon::FA) ?><?= Yii::t('app', 'Dashboard') ?></a></li> 
         <li><a href="<?= Url::to(['/flat']); ?>"><?= Icon::show('home', [], Icon::FA) ?><?= Yii::t('app', 'Flats') ?></a></li>     
         <li><a href="<?= Url::to(['/tenant']); ?>"><?= Icon::show('user', [], Icon::FA) ?><?= Yii::t('app', 'Tenants') ?></a></li>
         <li>
            <a href="#" class="expand"><?= Icon::show('money', [], Icon::FA) ?><?= Yii::t('app', 'Finance') ?><?= Icon::show('bars', ['class'=>'pull-right'], Icon::FA) ?></a>
            <ul>
               <li><a href="visuals.html">Visuals notifications</a></li>
               <li><a href="navs.html">Navs navbars</a></li>
               <li><a href="panel_options.html">Panels</a></li>
               <li><a href="icons.html">Icons <span class="label label-danger">190</span></a></li>
               <li><a href="buttons.html">Buttons</a></li>
               <li><a href="content_grid.html">Content grid</a></li>
            </ul>
         </li>
         <li><a href="<?= Url::to(['/']); ?>"><?= Icon::show('file-text', [], Icon::FA) ?><?= Yii::t('app', 'Documents') ?></a></li>   
         <li>
            <a href="#" class="expand"><?= Icon::show('cog', [], Icon::FA) ?><?= Yii::t('app', 'Settings') ?><?= Icon::show('bars', ['class'=>'pull-right'], Icon::FA) ?></a>
            <ul >
               <li><a href="form_components.html">Form components</a></li>
               <li><a href="form_validation.html">Form validation</a></li>
               <li><a href="wysiwyg.html">WYSIWYG editor</a></li>
               <li><a href="form_layouts.html">Form layouts</a></li>
               <li><a href="form_grid.html">Inputs grid</a></li>
               <li><a href="file_uploader.html">Multiple file uploader</a></li>
            </ul>
         </li>
        
      </ul>
   </div>
   <?php } ?>

   <div class="page-<?= Yii::$app->user->can('manageOwnFlats') ? 'content': '' ?>">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>

      <footer class="footer">
         <div class="container-fluid">
            <p class="pull-left">&copy; In Motion <?= date('Y') ?></p>
            <p class="pull-right">Flat management 0.9.0</p>
         </div>
      </footer>  

   </div>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
