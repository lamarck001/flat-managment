<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;


use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;

/**
 * @var yii\web\View $this
 * @var app\models\Tenant $model
 * @var yii\widgets\ActiveForm $form
 */

//echo '<pre>'; print_r($allFlats ); echo '</pre>'; 

?>

<div class="tenant-form">

   <?php $form = ActiveForm::begin([
         'type'=>ActiveForm::TYPE_HORIZONTAL
      ]);
   ?>

   <?php 
      $model->type = 1;
      echo $form->field($model, 'type')->hiddenInput(['value'=>1])->label(false);
      echo $form->field($model, 'user_id')->hiddenInput(['value'=>Yii::$app->user->id])->label(false);
   ?>

   <?= $form->field($model, 'subject_type')->widget(Select2::classname(), [
         'data'=>[
            1=>Yii::t('app', 'Person'),
            2=>Yii::t('app', 'Company'),
         ]         
      ]);  
   ?>
   
   <div id="company">   
      <?php
         echo $form->field($model, 'company_name')->textInput();
         echo $form->field($model, 'company_id')->textInput();
         echo $form->field($model, 'vat_id')->textInput();
         echo $form->field($model, 'web')->textInput();
      ?>
   </div>

   <div id="person"> 

      <?php 
         echo $form->field($model, 'flat_id')->widget(Select2::classname(), [
         'data'=>ArrayHelper::map($model->allFlats, 'id', 'title') ,
      ]);  
         echo $form->field($model, 'name')->textInput();
         echo $form->field($model, 'surname')->textInput();         
         echo $form->field($model, 'id_type')->widget(Select2::classname(), [
            'data'=>$model->getIdType()            
         ]);         
         echo $form->field($model, 'id_number')->textInput();
         echo $form->field($model, 'date_of_birth')->widget(DateControl::classname(), []); 
         echo $form->field($model, 'sex')->widget(Select2::classname(), [
            'data'=>$model->getSex(), 
         ]);
      ?>
   </div>

   <div id="contact">
   <?php
      echo $form->field($model, 'phone')->textInput();
      echo $form->field($model, 'email')->textInput();
   ?>
   </div>

   <div id="address">
      <?php
         echo $form->field($model, 'address_street')->textInput();
         echo $form->field($model, 'address_street_number')->textInput();
         echo $form->field($model, 'address_city')->textInput();
         echo $form->field($model, 'address_zip')->textInput();
         echo $form->field($model, 'address_state')->textInput();
      ?>
   </div>
         

   <div id="other">
      <?php
         echo $form->field($model, 'start_date')->textInput();
         echo $form->field($model, 'end_date')->textInput();
         echo $form->field($model, 'note')->textArea();
         echo $form->field($model, 'status')->widget(SwitchInput::classname(), []);
      ?>
   </div>







  

   <?php /* echo Form::widget([
      'model' => $model,
      'form' => $form,
      'columns' => 1,
      'attributes' => [
         'type'=>[
            'type'=> Form::INPUT_TEXT,
            'options'=>[
               'value'=>1,
               'hidden'=>'hidden'

            ]
         ],
         'company_name'=>[
            'type'=> Form::INPUT_TEXT,
         ],    
         'company_id'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'vat_id'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'web'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'phone'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'email'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'name'=>[
            'type'=> Form::INPUT_TEXT, 
            'options'=>[
               'placeholder'=>'Enter Name...', 
               'maxlength'=>255
            ]
         ], 
         'surname'=>[
            'type'=> Form::INPUT_TEXT, 
            'options'=>[
               'placeholder'=>'Enter Surname...', 
               'maxlength'=>255
            ]
         ], 
         'date_of_birth'=>[
            'type'=> Form::INPUT_WIDGET, 
            'widgetClass'=>DateControl::classname(),
            //'format'=>['date','php:d.m.Y'],
         ], 
         'sex'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\kartik\widgets\Select2',
            'options'=>[
               'data'=>$model->getSex()
            ], 
            'hint'=>'Type and select state'
         ],
         'address_street'=>[
            'type'=> Form::INPUT_TEXT,
         ], 
         'address_street_number'=>[
            'type'=> Form::INPUT_TEXT,
         ], 
         'address_city'=>[
            'type'=> Form::INPUT_TEXT,
         ], 
         'address_zip'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'address_state'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'id_type'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\kartik\widgets\Select2',
            'options'=>[
               'data'=>$model->getIdType()
            ], 
         ],
         'id_number'=>[
            'type'=> Form::INPUT_TEXT, 
            'options'=>[
               'placeholder'=>'Enter Id Number...', 
               'maxlength'=>100
            ]
         ], 
         'flat_id'=>[
            'type'=>Form::INPUT_WIDGET, 
            'widgetClass'=>'\kartik\widgets\Select2',
            'options'=>[
               'data'=> ArrayHelper::map($model->allFlats, 'id', 'title')
            ], 
         ],
         'start_date'=>[
            'type'=> Form::INPUT_WIDGET, 
            'widgetClass'=>DateControl::classname(),
            'options'=>[
               'type'=>DateControl::FORMAT_DATE
            ]
         ], 
         'end_date'=>[
            'type'=> Form::INPUT_WIDGET, 
            'widgetClass'=>DateControl::classname(),
            'options'=>[
               'type'=>DateControl::FORMAT_DATE
            ]
         ], 
         'status'=>[
            'type'=> Form::INPUT_TEXT,
         ],
         'note'=>[
            'type'=> Form::INPUT_TEXTAREA,
         ]
       ]
    ]);*/

   echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);

   ActiveForm::end(); ?>
</div>
