<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\TenantSearch $searchModel
 */




$this->title = Yii::t('app', 'Subjects');
$this->params['breadcrumbs'][] = $this->title;




?>
<div class="tenant-index">
    <div class="page-header">
            <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Tenant',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); 

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],  
            [  
               'attribute'=>'subject_type',
               'value'=>function ($model, $key, $index, $column) {
                  $subjectType  = $model->getSubjectTypes();
                  return $subjectType[$model->subject_type];
               } 

            ],
            'name',
            'surname',
            [
               'attribute'=>'date_of_birth',
               'format'=>[
                  'date','php:d.m.Y'
               ]
            ],
            [
               'attribute'=>'sex',
               'value'=> function ($model, $key, $index, $column) {
                  $sex  = $model->getSex();
                  return $sex[$model->sex];
               }       
            ],
            [
               'attribute'=> 'id_type',
               'value'=> function ($model, $key, $index, $column) {
           
                  $idType  = $model->getIdType();
                  return $idType[$model->id_type];
               }   
            ],
            'id_number',  
            ['attribute'=>'start_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 
           ['attribute'=>'end_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}{delete}',
                'buttons' => [
               /*'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['tenant/view','id' => $model->id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}*/

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); 

    Pjax::end(); ?>

</div>
