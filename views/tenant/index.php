<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use kartik\icons\Icon;
Icon::map($this, Icon::FA);
Icon::map($this, Icon::EL);


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\TenantSearch $searchModel
 */

$this->title = Yii::t('app', 'Tenants');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tenant-index">
   
    <?php Pjax::begin(); 

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],      
            'name',
            'surname',
            [
               'attribute'=>'date_of_birth',
               'format'=>[
                  'date','php:d.m.Y'
               ]
            ],
            [
               'attribute'=>'sex',
               'value'=> function ($model, $key, $index, $column) {
                  $sex  = $model->getSex();
                  return $sex[$model->sex];
               }       
            ],
            [
               'attribute'=> 'id_type',
               'value'=> function ($model, $key, $index, $column) {
           
                  $idType  = $model->getIdType();
                  return $idType[$model->id_type];
               }   
            ],
            'id_number',         
            [
               'attribute'=>'start_date',
               'format'=>[
                  'date',
                  (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'
               ]
            ], 
            [
               'attribute'=>'end_date',
               'format'=>[
                  'date',
                  (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'
               ]
            ], 
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}{delete}',
                'buttons' => [
                /*'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['tenant/view','id' => $model->id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}*/
                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,
        'panel' => [            
            'heading'=>Icon::show('user', [], Icon::BSG).Html::encode($this->title),
            //'type'=>'info',
            'before'=>Html::a(Icon::show('plus', [], Icon::BSG).'Add', ['create'], ['class' => 'btn btn-success']),
            'after'=>Html::a(Icon::show('repeat', [], Icon::BSG).'Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); 

    Pjax::end(); ?>

</div>
