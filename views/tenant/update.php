<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Tenant $model
 */

$this->title = Yii::t('app', 'Tenant: {tenant}: ', [
    'tenant' => $model->name .' '.$model->surname,
]);


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tenants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name .' '.$model->surname];
?>
<div class="tenant-update">

    <h1><?= Html::encode($model->name .' '.$model->surname) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
