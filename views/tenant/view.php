<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Tenant $model
 */


$sex = [
   '1'=>'male',
   '2'=>'female'
];


$this->title = $model->name.' '.$model->surname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tenants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name.' '.$model->surname;
?>
<div class="tenant-view">
    <div class="page-header">
        <h1><?= Html::encode($model->name.' '.$model->surname) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            //'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'mode'=> DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
      /*  'attributes' => [
            'id',
            'title',
            'name',
            'surname',
            [
                'attribute'=>'birthday',
                'format'=>[
                  'date','php:d.m.Y'
               ],
                'type'=>DetailView::INPUT_WIDGET,
                'widgetOptions'=> [
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATE,
                ]
            ],
            [
               'attribute'=>'sex',
               'value'=> $model->sex == 1 ? 'male' : 'female'  
            ], 
            'id_type',
            'id_number',
            'flat_id',
            [
                'attribute'=>'start_date',
                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
                'type'=>DetailView::INPUT_WIDGET,
                'widgetOptions'=> [
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATE
                ]
            ],
            [
                'attribute'=>'end_date',
                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
                'type'=>DetailView::INPUT_WIDGET,
                'widgetOptions'=> [
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATE
                ]
            ],
        ],*/
        'deleteOptions'=>[
            'url'=>['delete', 'id' => $model->id],
            'data'=>[
               'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
               'method'=>'post',
            ],
        ],
        'enableEditMode'=>false,
    ]) ?>

</div>
